# irrverbsscr.herokuapp.com

English Irregular Verbs: a Screen Saver, a Slide Show, a Presentation, a Table

[![irrverbsscr.herokuapp.com](http://farm8.staticflickr.com/7659/27893463622_7205ae099c_o.jpg)](https://irrverbsscr.herokuapp.com/)

## On-line

 - [the website](https://irrverbsscr.herokuapp.com/)
 
## Dashboard

<https://dashboard.heroku.com/apps/irrverbsscr>
 
## Production Push URL

```
https://git.heroku.com/irrverbsscr.git
```

## Remotes

 - [GitHub](https://github.com/englishextra/irrverbsscr.herokuapp.com)
 - [BitBucket](https://bitbucket.org/englishextra/irrverbsscr.herokuapp.com)
 - [GitLab](https://gitlab.com/englishextra/irrverbsscr.herokuapp.com)
