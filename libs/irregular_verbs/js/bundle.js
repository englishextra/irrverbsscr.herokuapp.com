/*!
 * safe way to handle console.log():
 * sitepoint.com/safe-console-log/
 */
"undefined"===typeof console&&(console={log:function(){}});
/*!
 * detect Electron and NW.js
 */
var isElectron="undefined"!==typeof window&&window.process&&"renderer"===window.process.type||"",
isNwjs="";try{"undefined"!==typeof require("nw.gui")&&(isNwjs=!0)}catch(a){isNwjs=!1};
var isOldOpera=!!window.opera||!1;
/*!
 * modified for babel ToProgress v0.1.1
 * http://github.com/djyde/ToProgress
 * arguments.callee changed to ToProgress
 * wrapped in curly brackets:
 * else{document.body.appendChild(this.progressBar);}
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * added if("undefined"==typeof window||!("document"in window))
 * {return console.log("window is undefined or document is not in window"),!1;}
 */
var ToProgress=(function(){if("undefined"==typeof window||!("document"in window)){return console.log("window is undefined or document is not in window"),!1;}function t(){var t,s=document.createElement("fakeelement"),i={transition:"transitionend",OTransition:"oTransitionEnd",MozTransition:"transitionend",WebkitTransition:"webkitTransitionEnd"};for(t in i)if(void 0!==s.style[t])return i[t]}function s(t,s){if(this.progress=0,this.options={id:"top-progress-bar",color:"#F44336",height:"2px",duration:.2},t&&"object"==typeof t)for(var i in t)this.options[i]=t[i];if(this.options.opacityDuration=3*this.options.duration,this.progressBar=document.createElement("div"),this.progressBar.id=this.options.id,this.progressBar.setCSS=function(t){for(var s in t)this.style[s]=t[s]},this.progressBar.setCSS({position:s?"relative":"fixed",top:"0",left:"0",right:"0","background-color":this.options.color,height:this.options.height,width:"0%",transition:"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s","-moz-transition":"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s","-webkit-transition":"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s"}),s){var o=document.querySelector(s);o&&(o.hasChildNodes()?o.insertBefore(this.progressBar,o.firstChild):o.appendChild(this.progressBar))}else document.body.appendChild(this.progressBar)}var i=t();return s.prototype.transit=function(){this.progressBar.style.width=this.progress+"%"},s.prototype.getProgress=function(){return this.progress},s.prototype.setProgress=function(t,s){this.show(),this.progress=t>100?100:0>t?0:t,this.transit(),s&&s()},s.prototype.increase=function(t,s){this.show(),this.setProgress(this.progress+t,s)},s.prototype.decrease=function(t,s){this.show(),this.setProgress(this.progress-t,s)},s.prototype.finish=function(t){var s=this;this.setProgress(100,t),this.hide(),i&&this.progressBar.addEventListener(i,function(t){s.reset(),s.progressBar.removeEventListener(t.type,ToProgress)})},s.prototype.reset=function(t){this.progress=0,this.transit(),t&&t()},s.prototype.hide=function(){this.progressBar.style.opacity="0"},s.prototype.show=function(){this.progressBar.style.opacity="1"},s;}());
/*!
 * init ToProgress
 */
var toprogress_options = {
	id : "top-progress-bar",
	color : "#FC6054",
	height : "3px",
	duration : .2
}, progressBar = new ToProgress(toprogress_options),
progressBarAvailable = "undefined" !== typeof window && window.progressBar ? !0 : !1,
startProgressBar = function (v) {
	v = v || 50;
	progressBarAvailable && progressBar.increase(v);
},
finishProgressBar = function () {
	progressBarAvailable && (progressBar.finish(), progressBar.hide());
};
progressBarAvailable && progressBar.increase(20);
/*!
 * modified MediaHack - (c) 2013 Pomke Nohkan MIT LICENCED.
 * gist.github.com/englishextra/ff8c9dde94abe32a9d7c4a65e0f2ccac
 * removed className fallback and additionally
 * returns earlyDeviceOrientation,earlyDeviceSize
 * Add media query classes to DOM nodes
 * github.com/pomke/mediahack/blob/master/mediahack.js
 */
var earlyDeviceOrientation="",earlyDeviceSize="";(function(d){function n(i){var n=i.split(" ");if(d){for(var i,e=0;e<n.length;e++){(i=n[e])&&d.add(i);}}}function l(i){var n=i.split(" ");if(d){for(var i,e=0;e<n.length;e++){(i=n[e])&&d.remove(i);}}}var i={landscape:"all and (orientation:landscape)",portrait:"all and (orientation:portrait)"};var j={small:"all and (max-width:768px)",medium:"all and (min-width:768px) and (max-width:991px)",large:"all and (min-width:992px)"};for(var e in i){var o=window.matchMedia(i[e]);!function(i,e){var o=function(i){i.matches?(n(e),(earlyDeviceOrientation=e)):l(e);};o(i),i.addListener(o);}(o,e);}for(var e in j){var s=window.matchMedia(j[e]);!function(j,e){var s=function(j){j.matches?(n(e),(earlyDeviceSize=e)):l(e);};s(j),j.addListener(s);}(s,e);}}(document.documentElement.classList||""));
/*!
 * add mobile or desktop class
 * using Detect Mobile Browsers | Open source mobile phone detection
 * Regex updated: 1 August 2014
 * detectmobilebrowsers.com
 */
var earlyDeviceType;(function(a,b,c,n){var c=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(n)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(n.substr(0,4))?b:c;a&&c&&(a.className+=" "+c),(earlyDeviceType=c)}(document.getElementsByTagName("html")[0]||"","mobile","desktop",navigator.userAgent||navigator.vendor||window.opera));
/*!
 * add svg support class
 */
var earlySvgSupport;(function(a,b){var c=document.implementation.hasFeature("http://www.w3.org/2000/svg","1.1")?b:"no-"+b;(earlySvgSupport=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","svg"));
/*!
 * add svgasimg support class
 */
var earlySvgasimgSupport;(function(a,b){var c=document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")?b:"no-"+b;(earlySvgasimgSupport=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","svgasimg"));
/*!
 * add touch support class
 */
var earlyHasTouch;(function(a,b){var c="ontouchstart" in document.documentElement?b:"no-"+b;(earlyHasTouch=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","touch"));
/*!
 * return date in YYYY-MM-DD format
 */
var earlyFnGetYyyymmdd=function(){"use strict";var a=new Date,b=a.getDate(),c=a.getMonth(),c=c+1,d=a.getFullYear();10>b&&(b="0"+b);10>c&&(c="0"+c);return d+"-"+c+"-"+b;};
/*!
 * Escape strings for use as JavaScript string literals
 * gist.github.com/englishextra/3053a4dc18c2de3c80ce7d26207681e0
 * modified github.com/joliss/js-string-escape
 */
var jsStringEscape=function(s){return(""+s).replace(/["'\\\n\r\u2028\u2029]/g,function(a){switch(a){case '"':case "'":case "\\":return"\\"+a;case "\n":return"\\n";case "\r":return"\\r";case "\u2028":return"\\u2028";case "\u2029":return"\\u2029"}})};
/*!
 * append details to title
 */
var initialDocumentTitle=document.title||"",
userBrowsingDetails=" ["+(earlyFnGetYyyymmdd()?earlyFnGetYyyymmdd():"")+(earlyDeviceType?" "+earlyDeviceType:"")+(earlyDeviceSize?" "+earlyDeviceSize:"")+(earlyDeviceOrientation?" "+earlyDeviceOrientation:"")+(earlySvgSupport?" "+earlySvgSupport:"")+(earlySvgasimgSupport?" "+earlySvgasimgSupport:"")+(earlyHasTouch?" "+earlyHasTouch:"")+"]";
document.title&&(document.title=jsStringEscape(document.title+userBrowsingDetails));
/*!
 * modified JavaScript Sync/Async forEach - v0.1.2 - 1/10/2012
 * github.com/cowboy/javascript-sync-async-foreach
 * Copyright (c) 2012 "Cowboy" Ben Alman; Licensed MIT
 * removed Node.js / browser support wrapper function
 * forEach(a,function(e){console.log("eachCallback: "+e);},!1});
 * forEach(a,function(e){console.log("eachCallback: "+e);},function(){console.log("doneCallback");});
 */
var forEach=function(a,b,c){var d=-1,e=a.length>>>0;(function f(g){var h,j=g===!1;do++d;while(!(d in a)&&d!==e);if(j||d===e){c&&c(!j,a);return}g=b.call({async:function(){return h=!0,f}},a[d],d,a),h||f(g)})()};
/*!
 * A function for elements selection
 * 0.1.9
 * github.com/finom/bala
 * global $ renamed to BALA, prepended var
 * a = BALA.one("#someid");
 * a = BALA.one(".someclass");
 * a = BALA(".someclass");
 */
var BALA=function(e,f,g){function c(a,b,d){d=Object.create(c.fn);a&&d.push.apply(d,a[f]?[a]:""+a===a?/</.test(a)?((b=e.createElement(b||f)).innerHTML=a,b.children):b?(b=c(b)[0])?b[g](a):d:e[g](a):"function"==typeof a?e.readyState[7]?a():e[f]("DOMContentLoaded",a):a);return d}c.fn=[];c.one=function(a,b){return c(a,b)[0]||null};return c}(document,"addEventListener","querySelectorAll");
/*!
 * Behaves the same as setTimeout except uses requestAnimationFrame() where possible for better performance
 * modified gist.github.com/joelambert/1002116
 * the fallback function requestAnimFrame is incorporated
 * gist.github.com/joelambert/1002116
 * gist.github.com/englishextra/873c8f78bfda7cafc905f48a963df07b
 * @param {function} fn The callback function
 * @param {int} delay The delay in milliseconds
 */
window.requestTimeout=function(fn,delay){if(!window.requestAnimationFrame&&!window.webkitRequestAnimationFrame&&!(window.mozRequestAnimationFrame&&window.mozCancelRequestAnimationFrame)&&!window.oRequestAnimationFrame&&!window.msRequestAnimationFrame){return window.setTimeout(fn,delay);};var requestAnimFrame=function(callback,element){window.setTimeout(callback,1000/60);},start=new Date().getTime(),handle=new Object();function loop(){var current=new Date().getTime(),delta=current-start;delta>=delay?fn.call():handle.value=requestAnimFrame(loop);};handle.value=requestAnimFrame(loop);return handle;};
/*!
 * Behaves the same as clearTimeout except uses cancelRequestAnimationFrame() where possible for better performance
 * gist.github.com/joelambert/1002116
 * gist.github.com/englishextra/873c8f78bfda7cafc905f48a963df07b
 * @param {int|object} fn The callback function
 */
window.clearRequestTimeout=function(handle){window.cancelAnimationFrame?window.cancelAnimationFrame(handle.value):window.webkitCancelAnimationFrame?window.webkitCancelAnimationFrame(handle.value):window.webkitCancelRequestAnimationFrame?window.webkitCancelRequestAnimationFrame(handle.value):window.mozCancelRequestAnimationFrame?window.mozCancelRequestAnimationFrame(handle.value):window.oCancelRequestAnimationFrame?window.oCancelRequestAnimationFrame(handle.value):window.msCancelRequestAnimationFrame?window.msCancelRequestAnimationFrame(handle.value):clearTimeout(handle);};
/*!
 * set and clear timeout
 * based on requestTimeout and clearRequestTimeout
 */
var setAndClearTimeout=function(fn,delay){delay=delay||500;if(!!fn&&"function"===typeof fn){var sct=requestTimeout(function(){clearRequestTimeout(sct);fn();},delay);}}; 
/*!
 * modified for babel crel - a small, simple, and fast DOM creation utility
 * github.com/KoryNunn/crel
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * crel(tagName/dom element [, attributes, child1, child2, childN...])
 * var element=crel('div',crel('h1','Crello World!'),crel('p','This is crel'),crel('input',{type:'number'}));
 */
var crel=(function(){function e(){var o,a=arguments,p=a[0],m=a[1],x=2,v=a.length,b=e[f];if(p=e[c](p)?p:d.createElement(p),1===v)return p;if((!l(m,t)||e[u](m)||s(m))&&(--x,m=null),v-x===1&&l(a[x],"string")&&void 0!==p[r])p[r]=a[x];else for(;v>x;++x)if(o=a[x],null!=o)if(s(o))for(var g=0;g<o.length;++g)y(p,o[g]);else y(p,o);for(var h in m)if(b[h]){var N=b[h];typeof N===n?N(p,m[h]):p[i](N,m[h])}else p[i](h,m[h]);return p}var n="function",t="object",o="nodeType",r="textContent",i="setAttribute",f="attrMap",u="isNode",c="isElement",d=typeof document===t?document:{},l=function(e,n){return typeof e===n},a=typeof Node===n?function(e){return e instanceof Node}:function(e){return e&&l(e,t)&&o in e&&l(e.ownerDocument,t)},p=function(n){return e[u](n)&&1===n[o]},s=function(e){return e instanceof Array},y=function(n,t){e[u](t)||(t=d.createTextNode(t)),n.appendChild(t)};return e[f]={},e[c]=p,e[u]=a,"undefined"!=typeof Proxy&&(e.proxy=new Proxy(e,{get:function(n,t){return!(t in e)&&(e[t]=e.bind(null,t)),e[t]}})),e}());
/*!
 * implementing fadeIn and fadeOut without jQuery
 * gist.github.com/englishextra/baaa687f6ae9c7733d560d3ec74815cd
 * modified jsfiddle.net/LzX4s/
 * changed options.complete(); to:
 * function"==typeof options.complete && options.complete();
 * usage:
 * FX.fadeIn(document.getElementById('test'), {
 * 	duration: 2000,
 * 	complete: function() {
 * 		alert('Complete');
 * 	}
 * });
 */
(function(){var FX={easing:{linear:function(progress){return progress;},quadratic:function(progress){return Math.pow(progress,2);},swing:function(progress){return 0.5-Math.cos(progress*Math.PI)/2;},circ:function(progress){return 1-Math.sin(Math.acos(progress));},back:function(progress,x){return Math.pow(progress,2)*((x+1)*progress-x);},bounce:function(progress){for(var a=0,b=1,result;1;a+=b,b/=2){if(progress>=(7-4*a)/11){return-Math.pow((11-6*a-11*progress)/4,2)+Math.pow(b,2);}}},elastic:function(progress,x){return Math.pow(2,10*(progress-1))*Math.cos(20*Math.PI*x/3*progress);}},animate:function(options){var start=new Date;var id=setInterval(function(){var timePassed=new Date-start;var progress=timePassed/options.duration;if(progress>1){progress=1;};options.progress=progress;var delta=options.delta(progress);options.step(delta);if(progress==1){clearInterval(id);"function"==typeof options.complete&&options.complete();}},options.delay||10);},fadeOut:function(element,options){var to=1;this.animate({duration:options.duration,delta:function(progress){progress=this.progress;return FX.easing.swing(progress);},complete:options.complete,step:function(delta){element.style.opacity=to-delta;}});},fadeIn:function(element,options){var to=0;this.animate({duration:options.duration,delta:function(progress){progress=this.progress;return FX.easing.swing(progress);},complete:options.complete,step:function(delta){element.style.opacity=to+delta;}});}};window.FX=FX;}());
/*!
 * Plain javascript replacement for jQuery's .ready()
 * so code can be scheduled to run when the document is ready
 * github.com/jfriend00/docReady
 * docReady(function(){});
 * simple substitute by Christoph at stackoverflow.com/questions/8100576/how-to-check-if-dom-is-ready-without-a-framework
 * (function(){var a=document.readyState;"interactive"===a||"complete"===a?(function(){}()):setTimeout(arguments.callee,100)})();
 */
(function(funcName,baseObj){"use strict";funcName=funcName||"docReady";baseObj=baseObj||window;var readyList=[];var readyFired=false;var readyEventHandlersInstalled=false;function ready(){if(!readyFired){readyFired=true;for(var i=0;i<readyList.length;i++){readyList[i].fn.call(window,readyList[i].ctx);}readyList=[];}}function readyStateChange(){if(document.readyState==="complete"){ready();}}baseObj[funcName]=function(callback,context){if(readyFired){setTimeout(function(){callback(context);},1);return;}else{readyList.push({fn:callback,ctx:context});}if(document.readyState==="complete"||(!document.attachEvent&&document.readyState==="interactive")){setTimeout(ready,1);}else if(!readyEventHandlersInstalled){if(document.addEventListener){document.addEventListener("DOMContentLoaded",ready,false);window.addEventListener("load",ready,false);}else{document.attachEvent("onreadystatechange",readyStateChange);window.attachEvent("onload",ready);}readyEventHandlersInstalled=true;}}})("docReady",window);
/*!
 * modified for babel Evento - v1.0.0
 * by Erik Royall <erikroyalL@hotmail.com> (http://erikroyall.github.io)
 * changed this or window to self as argument
 * added if("undefined"==typeof window||!("document"in window))
 * {return console.log("window is undefined or document is not in window"),!1;}
 * Dual licensed under MIT and GPL
 * Array.prototype.indexOf shim
 * developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
 * gist.github.com/erikroyall/6618740
 * gist.github.com/englishextra/3a959e4da0fcc268b140
 * evento.add(window,"load",function(){});
 */
if(!Array.prototype.indexOf){Array.prototype.indexOf=function(searchElement){'use strict';if(this==null){throw new TypeError();}var n,k,t=Object(this),len=t.length>>>0;if(len===0){return-1;}n=0;if(arguments.length>1){n=Number(arguments[1]);if(n!=n){n=0;}else if(n!=0&&n!=Infinity&&n!=-Infinity){n=(n>0||-1)*Math.floor(Math.abs(n));}}if(n>=len){return-1;}for(k=n>=0?n:Math.max(len-Math.abs(n),0);k<len;k++){if(k in t&&t[k]===searchElement){return k;}}return-1;};}var evento=(function(){if("undefined"==typeof window||!("document"in window)){return console.log("window is undefined or document is not in window"),!1;}var win=window,doc=win.document,_handlers={},addEvent,removeEvent,triggerEvent;addEvent=(function(){if(typeof doc.addEventListener==="function"){return function(el,evt,fn){el.addEventListener(evt,fn,false);_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}else if(typeof doc.attachEvent==="function"){return function(el,evt,fn){el.attachEvent(evt,fn);_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}else{return function(el,evt,fn){el["on"+evt]=fn;_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}}());removeEvent=(function(){if(typeof doc.removeEventListener==="function"){return function(el,evt,fn){el.removeEventListener(evt,fn,false);Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}else if(typeof doc.detachEvent==="function"){return function(el,evt,fn){el.detachEvent(evt,fn);Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}else{return function(el,evt,fn){el["on"+evt]=undefined;Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}}());triggerEvent=function(el,evt){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];for(var _i=0,_l=_handlers[el][evt].length;_i<_l;_i+=1){_handlers[el][evt][_i]();}};return{add:addEvent,remove:removeEvent,trigger:triggerEvent,_handlers:_handlers};}());
/*!
 * DoSlide v1.1.3
 * (c) 2016 MopTym <moptym@163.com>
 * Released under the MIT License.
 * Homepage - https://github.com/MopTym/doSlide
 */
!function(t,e){"object"==typeof exports&&"object"==typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.DoSlide=e():t.DoSlide=e()}(this,function(){return function(t){function e(i){if(n[i])return n[i].exports;var r=n[i]={exports:{},id:i,loaded:!1};return t[i].call(r.exports,r,r.exports,e),r.loaded=!0,r.exports}var n={};return e.m=t,e.c=n,e.p="",e(0)}([function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}function r(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}var o=Object.assign||function(t){for(var e=1;e<arguments.length;e++){var n=arguments[e];for(var i in n)Object.prototype.hasOwnProperty.call(n,i)&&(t[i]=n[i])}return t},s=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),u=n(4),a=n(1),c=i(a),l=n(5),f=(n(2),n(3)),h=n(6),d=i(h),v=function(){function t(){var e=arguments.length<=0||void 0===arguments[0]?document.createElement("div"):arguments[0],n=arguments.length<=1||void 0===arguments[1]?{}:arguments[1];r(this,t),Object.defineProperty(this,"_data",u.DATA_DESCRIPTOR),this.$=c["default"],this.callbacks={onChanged:[],onBeforeChange:[],onOverRange:[],onUserMouseWheel:[],onUserSwipe:[]},this.userEvent=null,this.isChanging=!1,this.el=e.nodeType?e:document.querySelector(e),this.eventEl=null,this.sections=this.el.children,this.currentIndex=n.initIndex||0,this.currentSection=this.sections[this.currentIndex],this.config=o({},u.DEFAULT_CONFIG,u.DEFAULT_INIT_CONFIG),this.set(n),(0,l.init)(this)}return s(t,[{key:"set",value:function(t,e){var n=this.config;return"string"==typeof t?n[t]=e:o(n,t),this}},{key:"get",value:function(t){return this.config[t]}},{key:"next",value:function(){var t=this.config.infinite?(this.currentIndex+1)%this.el.children.length:this.currentIndex+1;return this.go(t),this}},{key:"prev",value:function(){var t=this.config.infinite?(this.currentIndex||this.el.children.length)-1:this.currentIndex-1;return this.go(t),this}},{key:"go",value:function(t){return(0,f.change)(this,t),this}},{key:"do",value:function(t){return t.call(this,this.currentIndex,this.currentSection),this}},{key:"onChanged",value:function(t){return this.callbacks.onChanged.push(t),this}},{key:"onBeforeChange",value:function(t){return this.callbacks.onBeforeChange.push(t),this}},{key:"onOverRange",value:function(t){return this.callbacks.onOverRange.push(t),this}},{key:"onUserMouseWheel",value:function(t){return this.callbacks.onUserMouseWheel.push(t),this}},{key:"onUserSwipe",value:function(t){return this.callbacks.onUserSwipe.push(t),this}},{key:"initSpaceByKey",value:function(t){return Object.defineProperty(this._data,t,{enumerable:!1,configurable:!0,writable:!1,value:{}}),this._data[t]}},{key:"getSpaceByKey",value:function(t){return this._data[t]}}]),t}();v.from=function(t,e,n){return new v(e,o({},t.config,n))},v.applyNewKey=function(){var t="key"+Date.now()+~~(1e4*Math.random());return t},v.use=function(t,e){t&&t.install&&t.install(v,e)},v.use(d["default"]),v.$=c["default"],v.supportedTransition=c["default"].getSupportedCSS("transition"),v.supportedTransform=c["default"].getSupportedCSS("transform"),t.exports=v},function(t,e){"use strict";function n(t){if(!t.length)return 0;var e=Array.prototype.reduce.call(t,function(t,e){return t+e});return e/t.length}function i(t){return"object"===("undefined"==typeof t?"undefined":a(t))&&o(t.length)}function r(t,e,n,i){return o(i)||"object"===("undefined"==typeof n?"undefined":a(n))?(h.each(t,function(t){return e(t,n,i)}),t):t.length?e(t[0],n):void 0}function o(t){return"undefined"!=typeof t}function s(t,e,n,i){if(t&&e)for(var r,o=0,s=t.length;s>o;o++)if(r=n?e.call(n,t[o],o,t):e(t[o],o,t),r===!1)return i}function u(t,e){var n=[];for(var i in t)n.push(e?e(i):i);return n}var a="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol?"symbol":typeof t},c=Object.assign||function(t){for(var e=1;e<arguments.length;e++){var n=arguments[e];for(var i in n)Object.prototype.hasOwnProperty.call(n,i)&&(t[i]=n[i])}return t};Object.defineProperty(e,"__esModule",{value:!0});var l=800,f=50,h=function d(t){return new d.prototype.Init(t)};h.prototype={constructor:h,length:0,Init:function(t){var e=this;return t?t instanceof h?t:(t.nodeType?(this[0]=t,this.length=1):("string"==typeof t&&(t=document.querySelectorAll(t)||[]),h.each(t,function(t,n){return e[n]=t}),this.length=t.length),this):this}},h.prototype.Init.prototype=h.prototype,c(h.prototype,{each:function(t,e,n,i){return h.each(this,t,e,n,i)},eq:function(t){return isNaN(t)?h():h(this[0>t?this.length+t:t])},on:function(t,e){var n=arguments.length<=2||void 0===arguments[2]?!1:arguments[2];return this.each(function(i){return h.on(i,t,e,n)})},off:function(t,e){var n=arguments.length<=2||void 0===arguments[2]?!1:arguments[2];return this.each(function(i){return h.off(i,t,e,n)})},attr:function(t,e){return r(this,h.attr,t)},css:function(t,e){return r(this,h.css,t,e)},removeAttr:function(t){return this.each(function(e){return h.removeAttr(e,t)})},addClass:function(t){return this.each(function(e){return h.addClass(e,t)})},removeClass:function(t){return this.each(function(e){return h.removeClass(e,t)})},hasClass:function(t){return!this.each(function(e){return!h.hasClass(e,t)},!1,!0,!1)},onMouseWheel:function(t,e){return this.each(function(n){return h.onMouseWheel(n,t,e)})},onSwipe:function(t,e){return this.each(function(n){return h.onSwipe(n,t,e)})}}),c(h,{each:function(t,e,n,r,o){if(i(t))for(var s,u=0,a=t.length;a>u;u++)if(s=n?e.call(t[u],t[u],u,t):e(t[u],u,t),s===!1&&r)return o;return t},on:function(t,e,n){var i=arguments.length<=3||void 0===arguments[3]?!1:arguments[3];t&&t.addEventListener(e,n,i)},off:function(t,e,n){var i=arguments.length<=3||void 0===arguments[3]?!1:arguments[3];t&&t.removeEventListener(e,n,i)},attr:function(t,e,n){if(t)if("string"==typeof e){if(!o(n))return t.getAttribute(e)||"";t.setAttribute(e,n)}else for(var i in e)t.setAttribute(i,e[i])},css:function(t,e,n){if(t&&e)if("string"==typeof e){if(!o(n))return t.style[e];t.style[e]=n}else for(var i in e)t.style[i]=e[i]},removeAttr:function(t,e){t&&t.removeAttribute(e)},addClass:function(t,e){if(t&&e&&!this.hasClass(t,e)){var n=this.attr(t,"class").trim(),i=(n+" "+e).trim();this.attr(t,"class",i)}},removeClass:function(t,e){if(t&&e){var n=new RegExp("\\s*\\b"+e+"\\b\\s*","g"),i=this.attr(t,"class").replace(n," ").trim();this.attr(t,"class",i)}},hasClass:function(t,e){return!(!t||!e)&&new RegExp("\\b"+e+"\\b").test(this.attr(t,"class"))}}),c(h,{getSupportedCSS:function(){var t=["","-webkit-","-moz-","-o-","-ms-"],e=document.createElement("div"),n=e.style;return function(e){var i=arguments.length<=1||void 0===arguments[1]?!0:arguments[1],r=i?t.map(function(t){return t+e}):[e],o=void 0;return s(r,function(t){return o=void 0!==n[t]?t:o,void 0===o}),o}}(),onMouseWheel:function(t,e){var i=arguments.length<=2||void 0===arguments[2]?function(){return!1}:arguments[2];if(t&&e){var r=0,o=[];["DOMMouseScroll","mousewheel"].map(function(s){t.addEventListener(s,function(s){s.preventDefault(),i()&&s.stopPropagation();var u=s.detail?-s.detail:s.wheelDelta;if(u){Date.now()-r>200&&(o=[]),r=Date.now(),o.push(Math.abs(u)),o.length>150&&o.shift();var a=~~n(o.slice(-10)),c=~~n(o.slice(-70)),l=a>=c;if(l){var f=0>u?"down":"up";e.call(t,f)}}},!1)})}},onSwipe:function(t,e){var n=arguments.length<=2||void 0===arguments[2]?function(){return!1}:arguments[2];if(t&&e){var i=void 0,r=void 0,o=void 0,s=void 0,u=void 0;t.addEventListener("touchstart",function(t){n()&&t.stopPropagation();var e=t.changedTouches[0];i=e.clientX,r=e.clientY,s=e.clientX,u=e.clientY,o=Date.now()},!1),t.addEventListener("touchmove",function(t){if(n()&&t.stopPropagation(),t.preventDefault(),(!t.scale||1===t.scale)&&1===t.changedTouches.length){var e=t.changedTouches[0];s=e.clientX,u=e.clientY}},!1),t.addEventListener("touchend",function(a){if(n()&&a.stopPropagation(),Date.now()-o<l){var c=s-i,h=u-r,d=Math.abs(c),v=Math.abs(h),g=void 0;Math.max(d,v)>f&&(g=d>v?c>0?"right":"left":h>0?"down":"up",e.call(t,g))}},!1)}},forEach:s,keys:u}),e["default"]=h},function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}function r(t){var e=t.userEvent;if(e){t.userEvent=null;var n=t.callbacks[e.name],i=s(n,e.args,t,!1);return i!==!1}return!0}function o(t,e){var n=t.callbacks[e.name],i=s(n,e.args,t,!1);return i!==!1}function s(t,e,n,i){return h["default"].forEach(t,function(t){return t.apply(n,e)},null,i)}function u(t){a(t),t.config.listenUserMouseWheel&&c(t,t.eventEl),t.config.listenUserSwipe&&l(t,t.eventEl)}function a(t){var e=t.config.eventElemSelector;null===e?t.eventEl=t.el:t.eventEl=e.nodeType?e:document.querySelector(e)}function c(t,e){h["default"].onMouseWheel(e,function(e){t.config.respondToUserEvent&&!t.isChanging&&(t.userEvent={name:"onUserMouseWheel",args:[e]},"down"===e?t.next():t.prev())},function(){return t.config.stopPropagation})}function l(t,e){h["default"].onSwipe(e,function(e){t.config.respondToUserEvent&&!t.isChanging&&(t.userEvent={name:"onUserSwipe",args:[e]},t.config.horizontal?("left"===e&&t.next(),"right"===e&&t.prev()):("up"===e&&t.next(),"down"===e&&t.prev()))},function(){return t.config.stopPropagation})}Object.defineProperty(e,"__esModule",{value:!0}),e.executeUserEventCallbacks=e.executeEventCallbacks=e.startListen=void 0;var f=n(1),h=i(f);e.startListen=u,e.executeEventCallbacks=o,e.executeUserEventCallbacks=r},function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}function r(t,e){var n=(0,v["default"])(t.el),i=(0,v["default"])(t.sections);t.config.horizontal?(n.css("width",i.length+"00%"),i.css({width:100/i.length+"%","float":"left"})):(n.css("height",i.length+"00%"),i.css("height",100/i.length+"%")),o(t,e,!0)}function o(t,e,n){var i=t.currentSection,r=t.sections[e],o=t.config,a=o.minInterval+(p?o.duration:0);return a=n?0:a,t.isChanging=!0,t.config.silent||(u(t,e),n||s(o,i,r,!0),h(t,e,n)),setTimeout(function(){o.silent||n||s(o,i,r,!1),t.isChanging=!1},a),a}function s(t,e,n,i){i?(v["default"].addClass(e,t.transitionOutClass),v["default"].addClass(n,t.transitionInClass)):(v["default"].removeClass(e,t.transitionOutClass),v["default"].removeClass(n,t.transitionInClass))}function u(t,e){(0,v["default"])(t.sections).each(function(n,i){i===e?v["default"].addClass(n,t.config.activeClass):v["default"].removeClass(n,t.config.activeClass)})}function a(t,e){c(t,e)&&(l(t,e)?f(t,e):(0,g.executeUserEventCallbacks)(t)&&!function(){var n=t.currentIndex,i=(0,g.executeEventCallbacks)(t,{name:"onBeforeChange",args:[n,e,t.currentSection,t.sections[e]]});if(i){var r=o(t,e);t.currentIndex=e,t.currentSection=t.sections[e],setTimeout(function(){(0,g.executeEventCallbacks)(t,{name:"onChanged",args:[e,n,t.currentSection,t.sections[n]]})},r)}}())}function c(t,e){return!t.isChanging&&e!=t.currentIndex}function l(t,e){return 0>e||e>=t.sections.length}function f(t,e){var n=t.config.parent,i=(0,g.executeEventCallbacks)(t,{name:"onOverRange",args:[t.currentIndex,e,t.currentSection]});i&&n&&(0>e?n.prev():n.next())}function h(t,e,n){var i=t.config;if(m){if(p){var r=m+" "+(i.timingFunction||"")+" "+i.duration+"ms",o=m+" 0ms";v["default"].css(t.el,p,n?o:r)}var s=100*-e/t.sections.length+"%",u=i.horizontal?s+",0":"0,"+s;u=y&&i.translate3d?"translate3d("+u+",0)":"translate("+u+")",v["default"].css(t.el,m,u)}else v["default"].css(t.el,i.horizontal?"left":"top",-e+"00%")}Object.defineProperty(e,"__esModule",{value:!0}),e.change=e.initSections=void 0;var d=n(1),v=i(d),g=n(2),p=v["default"].getSupportedCSS("transition"),m=v["default"].getSupportedCSS("transform"),y=function(){var t=!1;if(m&&window.getComputedStyle){var e=document.createElement("div");document.body.insertBefore(e,null),e.style[m]="translate3d(1%, 1%, 0)",t=window.getComputedStyle(e).getPropertyValue(m),document.body.removeChild(e)}return t&&"none"!==t}();e.initSections=r,e.change=a},function(t,e){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.DEFAULT_INIT_CONFIG={initIndex:0,initClass:"ds-init",activeClass:"active",transitionInClass:"transition-in",transitionOutClass:"transition-out",silent:!1,horizontal:!1,infinite:!1,listenUserMouseWheel:!0,listenUserSwipe:!0,eventElemSelector:null},e.DEFAULT_CONFIG={duration:1e3,timingFunction:"ease",minInterval:50,translate3d:!0,parent:null,respondToUserEvent:!0,stopPropagation:!1},e.DATA_DESCRIPTOR={enumerable:!1,configurable:!1,writable:!1,value:{}}},function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}function r(t){t.config.silent||(0,u.initSections)(t,t.config.initIndex||0),t.config.eventElemSelector!==!1&&(0,a.startListen)(t),s["default"].removeClass(t.el,t.config.initClass)}Object.defineProperty(e,"__esModule",{value:!0}),e.init=void 0;var o=n(1),s=i(o),u=n(3),a=n(2);e.init=r},function(t,e){"use strict";function n(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}function i(t){return function(e){return e.keyCode===t}}function r(t){var e=this.mappings||[],n=this["for"];e.forEach(function(e){e.filter.call(n,t)===!0&&e.action.call(n,t)})}function o(t){t.prototype.getKeyboard=function(){var e=t.applyNewKey();return function(){var t=this.getSpaceByKey(e);return t||(t=this.initSpaceByKey(e),t.res=new u(this,e)),t.res}}()}var s=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}();Object.defineProperty(e,"__esModule",{value:!0});var u=function(){function t(e,o){n(this,t),this.eventType="keydown",this.eventElement=window,this["for"]=e,this.$=e.$,this.isOn=!1,this.listener=r.bind(this),this.mappings=[{filter:i(40),action:function(){this.config.horizontal||this.next()}},{filter:i(38),action:function(){this.config.horizontal||this.prev()}},{filter:i(39),action:function(){this.config.horizontal&&this.next()}},{filter:i(37),action:function(){this.config.horizontal&&this.prev()}}]}return s(t,[{key:"setEventType",value:function(t){if(t!==this.eventType){var e=this.isOn;e&&this.turnOff(),this.eventType=t,e&&this.turnOn()}return this}},{key:"setEventElement",value:function(t){if(t!==this.eventElement){var e=this.isOn;e&&this.turnOff(),this.eventElement=t,e&&this.turnOn()}return this}},{key:"getMappings",value:function(){return this.mappings}},{key:"setMappings",value:function(t){return this.mappings=t,this}},{key:"turnOn",value:function(){return this.isOn||(this.$.on(this.eventElement,this.eventType,this.listener,!1),this.isOn=!0),this}},{key:"turnOff",value:function(){return this.isOn&&(this.$.off(this.eventElement,this.eventType,this.listener,!1),this.isOn=!1),this}}]),t}();e["default"]={install:o}}])});
/*!
 * How can I check if a JS file has been included already?
 * stackoverflow.com/questions/18155347/how-can-i-check-if-a-js-file-has-been-included-already
 */
var scriptIsLoaded=function(s){for(var b=document.getElementsByTagName("script")||"",a=0;a<b.length;a++)if(b[a].getAttribute("src")==s)return!0;return!1};
/*!
 * Load and execute JS via AJAX
 * gist.github.com/englishextra/8dc9fe7b6ff8bdf5f9b483bf772b9e1c
 * IE 5.5+, Firefox, Opera, Chrome, Safari XHR object
 * gist.github.com/Xeoncross/7663273
 * modified callback(x.responseText,x); to callback(eval(x.responseText),x);
 * stackoverflow.com/questions/3728798/running-javascript-downloaded-with-xmlhttprequest
 */
var AJAXLoadAndTriggerJs=function(u,cb,d){var w=window;try{var x=w.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");x.open(d?"POST":"GET",u,!0);x.setRequestHeader("X-Requested-With","XMLHttpRequest");x.setRequestHeader("Content-type","application/x-www-form-urlencoded");x.onreadystatechange=function(){if(x.readyState>3){if(x.responseText){eval(x.responseText);cb&&"function"===typeof cb&&cb(x.responseText);}}};x.send(d);}catch(e){console.log("Error XMLHttpRequest-ing file",e);}};
/*!
 * remove all children of parent element
 */
var removeChildElements=function(a){if(a)for(;a.firstChild;)a.removeChild(a.firstChild);};
/*!
 * set style display block of an element
 */
var setStyleDisplayBlock=function(a){a&&(a.style.display="block");};
/*!
 * set style display none of an element
 */
var setStyleDisplayNone=function(a){a&&(a.style.display="none");};
/*!
 * toggle style display of an element
 */
var toggleStyleDisplay=function(a,show,hide){if(a){a.style.display=hide==a.style.display||""==a.style.display?show:hide;}};
/*!
 * set style opacity of an element
 */
var setStyleOpacity=function(a,n){a&&(a.style.opacity=n);};
/*!
 * set style visibility visible of an element
 */
var setStyleVisibilityVisible=function(a){a&&(a.style.visibility="visible");};
/*!
 * get http or https
 */
var getHTTPProtocol=function(){var a=window.location.protocol||"";return"http:"===a?"http":"https:"===a?"https":""};
/*!
 * show data loading spinner
 */
var showDataLoadingSpinner = function () {
	var h = BALA.one("html") || "",
	data_loading = "data-loading";
	h && h.classList.add(data_loading);
};
/*!
 * hide data loading spinner
 */
var hideDataLoadingSpinner = function () {
	var h = BALA.one("html") || "",
	data_loading = "data-loading";
	if (h) {
		setAndClearTimeout(function () {
			h.classList.remove(data_loading);
		}, 500);
	}
};
/*!
 * Open external links in default browser out of Electron / nwjs
 * gist.github.com/englishextra/b9a8140e1c1b8aa01772375aeacbf49b
 * stackoverflow.com/questions/32402327/how-can-i-force-external-links-from-browser-window-to-open-in-a-default-browser
 * github.com/nwjs/nw.js/wiki/shell
 * electron - file: | nwjs - chrome-extension: | http: Intel XDK
 */
var openDeviceBrowser = function (a) {
	var w = window,
	g = function () {
		var electronShell = require("electron").shell;
		electronShell.openExternal(a);
	},
	k = function () {
		var nwGui = require("nw.gui");
		nwGui.Shell.openExternal(a);
	},
	q = function () {
		var win = w.open(a, "_blank");
		win.focus();
	},
	v = function () {
		w.open(a, "_system", "scrollbars=1,location=no");
	};
	if (!!isElectron) {
		g();
	} else if (!!isNwjs) {
		k();
	} else {
		if (!!getHTTPProtocol()) {
			q();
		} else {
			v();
		}
	}
};
/*!
 * set target blank to external links
 */
var setTargetBlankOnAnchors = function () {
	var w = window,
	a = BALA("a") || "",
	g = function (e) {
		var p = e.getAttribute("href") || "";
		if (p
			&& (/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)|(localhost)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/.test(p) || /http:\/\/localhost/.test(p))
			&& !e.getAttribute("rel")) {
			crel(e, {
				"title" : "\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0432\u043d\u0435\u0448\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441 " + (p.match(/:\/\/(.[^/]+)/)[1] || "") + " \u043e\u0442\u043a\u0440\u043e\u0435\u0442\u0441\u044f \u0432 \u043d\u043e\u0432\u043e\u0439 \u0432\u043a\u043b\u0430\u0434\u043a\u0435 \u0438\u043b\u0438 \u043e\u043a\u043d\u0435"
			});
			if (w.openDeviceBrowser) {
				crel(e, {
					"onclick" : "openDeviceBrowser('" + jsStringEscape(p) + "');return !1;"
				});
			} else {
				crel(e, {
					"target" : "_blank"
				});
			}
		}
	},
	k = function () {
			if (w._) {
				_.each(a, function (e) {
					g(e);
				});
			} else if (w.forEach) {
				forEach(a, function (e) {
					g(e);
				}, !1);
			} else {
				for (var i = 0, l = a.length; i < l; i += 1) {
					g(a[i]);
				};
			}
	};
	if (a) {
		k();
	}
};
docReady(function () {
	setTargetBlankOnAnchors();
});
/*!
 * init fastclick
 * github.com/ftlabs/fastclick
 */
var initFastclick = function () {
	var w = window,
	b = BALA.one("body") || "",
	fastclick_js_src = "/cdn/fastclick/1.0.6/js/fastclick.fixed.min.js",
	g = function () {
		AJAXLoadAndTriggerJs(fastclick_js_src, function () {
			w.FastClick && FastClick.attach(b);
		});
	};
	if (!!getHTTPProtocol()) {
		g();
	}
};
"undefined" !== typeof earlyHasTouch && "touch" === earlyHasTouch && evento.add(window, "load", function () {
	initFastclick();
});
/*!
 * hide ui buttons in fullscreen mode
 */
var hideUiButtonsInFullscreen = function () {
	var w = window,
	cd_prev = BALA.one(".cd-prev") || "",
	cd_next = BALA.one(".cd-next") || "",
	btn_nav_menu = BALA.one(".btn-nav-menu") || "",
	btn_menu_more = BALA.one(".btn-menu-more") || "",
	btn_show_vk_like = BALA.one(".btn-show-vk-like") || "",
	openapi_js_src = getHTTPProtocol() + "://vk.com/js/api/openapi.js?105",
	vk_like = BALA.one(".vk-like") || "",
	btn_block_social_buttons = BALA.one(".btn-block-social-buttons") || "",
	ui_totop = BALA.one("#ui-totop") || "",
	holder_search_form = BALA.one(".holder-search-form") || "",
	f = !1;
	if (!1 === f) {
		/*!
		 * Detecting if a browser is in full screen mode
		 * stackoverflow.com/questions/2863351/checking-if-browser-is-in-fullscreen
		 * stackoverflow.com/questions/1047319/detecting-if-a-browser-is-in-full-screen-mode
		 */
		var args = [cd_prev, cd_next, btn_nav_menu, btn_menu_more, btn_block_social_buttons, ui_totop, holder_search_form];
		if ((w.navigator.standalone) || (screen.height === w.outerHeight) || (w.fullScreen) || (w.innerWidth == screen.width && w.innerHeight == screen.height)) {
			for (var i = 0; i < args.length; i++) {
				setStyleDisplayNone(args[i]);
			}
			setStyleDisplayNone(btn_show_vk_like);
			f = !0;
		} else {
			for (var i = 0; i < args.length; i++) {
				setStyleDisplayBlock(args[i]);
			}
			scriptIsLoaded(openapi_js_src) || setStyleDisplayBlock(btn_show_vk_like);
			f = !0;
		}
	}
};
("undefined" !== typeof earlyDeviceSize && "medium" === earlyDeviceSize) || evento.add(window, "resize", function () {
	hideUiButtonsInFullscreen();
});
/*!
 * init DoSlide
 * A simple slider.
 * github.com/MopTym/doSlide/blob/master/demo/3_1_slider.html
 */
var initDoSlide = function () {
	var d = document,
	cd_prev = BALA.one(".cd-prev") || "",
	cd_next = BALA.one(".cd-next") || "",
	timer = function (slide, interval, token) {
		var next = function () {
			token = setAndClearTimeout(next, interval);
			if (!(d.hidden || d.webkitHidden)) {
				slide.next();
			}
		};
		return function () {
			clearTimeout(token);
			token = setAndClearTimeout(next, interval);
		}
	},
	slide = new DoSlide(".container", {
			duration : 2000,
			horizontal : true,
			infinite : true
		}),
	g = function () {
		if ("undefined" !== typeof slideTimer) {
			/* setStyleDisplayNone(cd_prev);
			setStyleDisplayNone(cd_next);
		} else { */
			setStyleDisplayBlock(cd_prev);
			setStyleDisplayBlock(cd_next);
			evento.add(cd_prev, "click", function (e) {
				e.preventDefault();
				e.stopPropagation();
				slide.prev();
			});
			evento.add(cd_next, "click", function (e) {
				e.preventDefault();
				e.stopPropagation();
				slide.next();
			});
		}
	};
	/*!
	 * dont JSMin line below: Notepad++ will freeze
	 * comment out if you dont want slide autorotation
	 */
	var slideTimer = timer(slide, 5000);
	slide.onChanged(slideTimer).do(slideTimer);
	/*!
	 * init next button if no slide autorotation
	 */
	if ((cd_next || cd_prev) && "undefined" !== typeof slide) {
		g();
	}
};
evento.add(window, "load", function () {
	initDoSlide();
});
/*!
 * init qr-code
 * stackoverflow.com/questions/12777622/how-to-use-enquire-js
 */
var showPageQRRef = function () {
	var w = window,
	d = document,
	a = BALA.one("#qr-code") || "",
	p = w.location.href || "",
	g = function () {
		removeChildElements(a);
		var t = jsStringEscape(d.title ? ("\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u00ab" + d.title.replace(/\[[^\]]*?\]/g, "").trim() + "\u00bb") : ""),
		s = getHTTPProtocol() + "://chart.googleapis.com/chart?cht=qr&chld=M|4&choe=UTF-8&chs=300x300&chl=" + encodeURIComponent(p),
		c = "width:10.000em;height:10.000em;background:transparent;background-size:120.000pt 120.000pt;border:0;vertical-align:bottom;padding:0;margin:0;";
		crel(a,
			crel("img", {
				"src" : s,
				"style" : c,
				"title" : t,
				"alt" : t
			}));
	};
	if (a && p) {
		if (!!getHTTPProtocol()) {
			g();
		} else {
			setStyleDisplayNone(a);
		}
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	showPageQRRef();
});
/*!
 * init nav-menu
 */
var initNavMenu = function () {
	var w = window,
	container = BALA.one("#container") || "",
	page = BALA.one("#page") || "",
	btn = BALA.one("#btn-nav-menu") || "",
	panel = BALA.one("#panel-nav-menu") || "",
	items = BALA("a", panel) || "",
	holder = BALA.one("#holder-panel-menu-more") || "",
	active = "active",
	p = w.location.href || "",
	g = function () {
		if (panel.classList.contains(active)) {
			page.classList.remove(active);
			panel.classList.remove(active);
			btn.classList.remove(active);
		}
	},
	k = function () {
		setStyleDisplayNone(holder);
		page.classList.toggle(active);
		panel.classList.toggle(active);
		btn.classList.toggle(active);
	},
	q = function () {
		setStyleDisplayNone(holder);
		page.classList.remove(active);
		panel.classList.remove(active);
		btn.classList.remove(active);
		/* scrollToTop(); */
	},
	m = function (e) {
		e.classList.remove(active);
	},
	n = function (e) {
		e.classList.add(active);
	},
	s = function (a) {
		if (w._) {
			_.each(a, function (e) {
				m(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				m(e);
			}, !1);
		} else {
			for (var j = 0, l = a.length; j < l; j += 1) {
				m(a[j]);
			};
		}
	},
	v = function (a, e) {
		evento.add(e, "click", function () {
			if (panel.classList.contains(active)) {
				q();
			}
			s(a);
			n(e);
		});
		if (e.href == p) {
			n(e);
		} else {
			m(e);
		}
	},
	z = function () {
		if (w._) {
			_.each(items, function (e) {
				v(items, e);
			});
		} else if (w.forEach) {
			forEach(items, function (e) {
				v(items, e);
			}, !1);
		} else {
			for (var i = 0, l = items.length; i < l; i += 1) {
				v(items, items[i]);
			};
		}
	};
	if (container && page && btn && panel && items) {
		/*!
		 * open or close nav
		 */
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			k();
		}),
		evento.add(container, "click", function () {
			g();
		});
		/*!
		 * close nav, scroll to top, highlight active nav item
		 */
		z();
	}
};
docReady(function () {
	initNavMenu();
});
/*!
 * init menu-more
 */
var initMenuMore = function () {
	var w = window,
	container = BALA.one("#container") || "",
	holder = BALA.one("#holder-panel-menu-more") || "",
	btn = BALA.one("#btn-menu-more") || "",
	panel = BALA.one("#panel-menu-more") || "",
	items = BALA("li", panel) || "",
	g = function (e) {
		evento.add(e, "click", function () {
			setStyleDisplayNone(holder);
		});
	},
	k = function () {
		evento.add(container, "click", function () {
			setStyleDisplayNone(holder);
		});
	},
	q = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			toggleStyleDisplay(holder, "inline-block", "none");
		});
	},
	v = function () {
		if (w._) {
			_.each(items, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(items, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = items.length; i < l; i += 1) {
				g(items[i]);
			};
		}
	};
	if (container && holder && btn && panel && items) {
		/*!
		 * hide menu more on outside click
		 */
		k();
		/*!
		 * show or hide menu more
		 */
		q();
		/*!
		 * hide menu more on item clicked
		 */
		v();
	}
};
docReady(function () {
	initMenuMore();
});
/*!
 * show menu-more on document ready
 */
var showMenuMoreOnLoad = function () {
	var a = BALA.one("#holder-panel-menu-more") || "",
	g = function () {
		setAndClearTimeout(function () {
			setStyleOpacity(a, 0),
			setStyleDisplayBlock(a),
			FX.fadeIn(a, {
				duration : 500
			});
			setAndClearTimeout(function () {
				FX.fadeOut(a, {
					duration : 500
				});
				setAndClearTimeout(function () {
					setStyleOpacity(a, 1),
					setStyleDisplayNone(a);
				}, 1000);
			}, 4000);
		}, 2000);
	};
	if (a) {
		g();
	}
};
docReady(function () {
	showMenuMoreOnLoad();
});
/*!
 * init pluso-engine or ya-share on click
 */
var showShareOptionsOnClick = function () {
	var pluso = BALA.one(".pluso") || "",
	ya_share2 = BALA.one(".ya-share2") || "",
	btn = BALA.one("#btn-block-social-buttons") || "",
	pluso_like_js_src = getHTTPProtocol() + "://share.pluso.ru/pluso-like.js",
	share_js_src = getHTTPProtocol() + "://yastatic.net/share2/share.js",
	g = function (share_block, btn) {
		setStyleVisibilityVisible(share_block);
		setStyleOpacity(share_block, 1);
		setStyleDisplayNone(btn);
	},
	k = function (js_src, share_block, btn) {
		scriptIsLoaded(js_src) || loadJS(js_src, function () {
			g(share_block, btn);
		});
	},
	q = function () {
		if (pluso) {
			k(pluso_like_js_src, pluso, btn);
		} else {
			if (ya_share2) {
				k(share_js_src, ya_share2, btn);
			}
		}
	},
	v = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			q();
		});
	};
	if ((pluso || ya_share2) && btn) {
		if (!!getHTTPProtocol()) {
			v();
		} else {
			setStyleDisplayNone(btn);
		}
	}
};
evento.add(window, "load", function () {
	showShareOptionsOnClick();
});
/*!
 * init vk-like on click
 */
var initVKOnClick = function () {
	var w = window,
	vk_like = BALA.one("#vk-like") || "",
	btn = BALA.one("#btn-show-vk-like") || "",
	openapi_js_src = getHTTPProtocol() + "://vk.com/js/api/openapi.js?122",
	g = function () {
		try {
			w.VK && (VK.init({
					apiId : (vk_like.dataset.apiid || ""),
					nameTransportPath : "/xd_receiver.htm",
					onlyWidgets : !0
				}), VK.Widgets.Like("vk-like", {
					type : "button",
					height : 24
				}));
			setStyleVisibilityVisible(vk_like);
			setStyleOpacity(vk_like, 1);
			setStyleDisplayNone(btn);
		} catch(e) {
			setStyleVisibilityHidden(vk_like);
			setStyleOpacity(vk_like, 0);
			setStyleDisplayBlock(btn);
		}
	},
	k = function () {
		scriptIsLoaded(openapi_js_src) || loadJS(openapi_js_src, function () {
			g();
		});
	}
	q = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			k();
		});
	};
	if (vk_like && btn) {
		if (!!getHTTPProtocol()) {
				q();
		} else {
			setStyleDisplayNone(btn);
		}
	}
};
evento.add(window, "load", function () {
	initVKOnClick();
});
/*!
 * init manUP.js
 */
var initManupJs = function () {
	var manup_js_src = "/cdn/ManUp.js/0.7/js/manup.fixed.min.js";
	if (!!getHTTPProtocol()) {
		AJAXLoadAndTriggerJs(manup_js_src);
	}
};
evento.add(window, "load", function () {
	initManupJs();
});
/*!
 * show page, finish ToProgress
 */
evento.add(window, "load", function () {
	var a = BALA.one("#container") || "";
	setStyleOpacity(a, 1);
	setImmediate(function () {
		progressBarAvailable && (progressBar.finish(), progressBar.hide());
	});
});
