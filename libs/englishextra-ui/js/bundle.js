/*!
 * safe way to handle console.log():
 * sitepoint.com/safe-console-log/
 */
"undefined"===typeof console&&(console={log:function(){}});
/*!
 * detect Electron and NW.js
 */
var isElectron="undefined"!==typeof window&&window.process&&"renderer"===window.process.type||"",
isNwjs="";try{"undefined"!==typeof require("nw.gui")&&(isNwjs=!0)}catch(a){isNwjs=!1};
var isOldOpera=!!window.opera||!1;
/*!
 * modified for babel ToProgress v0.1.1
 * http://github.com/djyde/ToProgress
 * arguments.callee changed to ToProgress
 * wrapped in curly brackets:
 * else{document.body.appendChild(this.progressBar);}
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * added if("undefined"==typeof window||!("document"in window))
 * {return console.log("window is undefined or document is not in window"),!1;}
 */
var ToProgress=(function(){if("undefined"==typeof window||!("document"in window)){return console.log("window is undefined or document is not in window"),!1;}function t(){var t,s=document.createElement("fakeelement"),i={transition:"transitionend",OTransition:"oTransitionEnd",MozTransition:"transitionend",WebkitTransition:"webkitTransitionEnd"};for(t in i)if(void 0!==s.style[t])return i[t]}function s(t,s){if(this.progress=0,this.options={id:"top-progress-bar",color:"#F44336",height:"2px",duration:.2},t&&"object"==typeof t)for(var i in t)this.options[i]=t[i];if(this.options.opacityDuration=3*this.options.duration,this.progressBar=document.createElement("div"),this.progressBar.id=this.options.id,this.progressBar.setCSS=function(t){for(var s in t)this.style[s]=t[s]},this.progressBar.setCSS({position:s?"relative":"fixed",top:"0",left:"0",right:"0","background-color":this.options.color,height:this.options.height,width:"0%",transition:"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s","-moz-transition":"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s","-webkit-transition":"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s"}),s){var o=document.querySelector(s);o&&(o.hasChildNodes()?o.insertBefore(this.progressBar,o.firstChild):o.appendChild(this.progressBar))}else document.body.appendChild(this.progressBar)}var i=t();return s.prototype.transit=function(){this.progressBar.style.width=this.progress+"%"},s.prototype.getProgress=function(){return this.progress},s.prototype.setProgress=function(t,s){this.show(),this.progress=t>100?100:0>t?0:t,this.transit(),s&&s()},s.prototype.increase=function(t,s){this.show(),this.setProgress(this.progress+t,s)},s.prototype.decrease=function(t,s){this.show(),this.setProgress(this.progress-t,s)},s.prototype.finish=function(t){var s=this;this.setProgress(100,t),this.hide(),i&&this.progressBar.addEventListener(i,function(t){s.reset(),s.progressBar.removeEventListener(t.type,ToProgress)})},s.prototype.reset=function(t){this.progress=0,this.transit(),t&&t()},s.prototype.hide=function(){this.progressBar.style.opacity="0"},s.prototype.show=function(){this.progressBar.style.opacity="1"},s;}());
/*!
 * init ToProgress
 */
var toprogress_options = {
	id : "top-progress-bar",
	color : "#FC6054",
	height : "3px",
	duration : .2
}, progressBar = new ToProgress(toprogress_options),
progressBarAvailable = "undefined" !== typeof window && window.progressBar ? !0 : !1,
startProgressBar = function (v) {
	v = v || 50;
	progressBarAvailable && progressBar.increase(v);
},
finishProgressBar = function () {
	progressBarAvailable && (progressBar.finish(), progressBar.hide());
};
progressBarAvailable && progressBar.increase(20);
/*!
 * modified MediaHack - (c) 2013 Pomke Nohkan MIT LICENCED.
 * gist.github.com/englishextra/ff8c9dde94abe32a9d7c4a65e0f2ccac
 * removed className fallback and additionally
 * returns earlyDeviceOrientation,earlyDeviceSize
 * Add media query classes to DOM nodes
 * github.com/pomke/mediahack/blob/master/mediahack.js
 */
var earlyDeviceOrientation="",earlyDeviceSize="";(function(d){function n(i){var n=i.split(" ");if(d){for(var i,e=0;e<n.length;e++){(i=n[e])&&d.add(i);}}}function l(i){var n=i.split(" ");if(d){for(var i,e=0;e<n.length;e++){(i=n[e])&&d.remove(i);}}}var i={landscape:"all and (orientation:landscape)",portrait:"all and (orientation:portrait)"};var j={small:"all and (max-width:768px)",medium:"all and (min-width:768px) and (max-width:991px)",large:"all and (min-width:992px)"};for(var e in i){var o=window.matchMedia(i[e]);!function(i,e){var o=function(i){i.matches?(n(e),(earlyDeviceOrientation=e)):l(e);};o(i),i.addListener(o);}(o,e);}for(var e in j){var s=window.matchMedia(j[e]);!function(j,e){var s=function(j){j.matches?(n(e),(earlyDeviceSize=e)):l(e);};s(j),j.addListener(s);}(s,e);}}(document.documentElement.classList||""));
/*!
 * add mobile or desktop class
 * using Detect Mobile Browsers | Open source mobile phone detection
 * Regex updated: 1 August 2014
 * detectmobilebrowsers.com
 */
var earlyDeviceType;(function(a,b,c,n){var c=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(n)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(n.substr(0,4))?b:c;a&&c&&(a.className+=" "+c),(earlyDeviceType=c)}(document.getElementsByTagName("html")[0]||"","mobile","desktop",navigator.userAgent||navigator.vendor||window.opera));
/*!
 * add svg support class
 */
var earlySvgSupport;(function(a,b){var c=document.implementation.hasFeature("http://www.w3.org/2000/svg","1.1")?b:"no-"+b;(earlySvgSupport=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","svg"));
/*!
 * add svgasimg support class
 */
var earlySvgasimgSupport;(function(a,b){var c=document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")?b:"no-"+b;(earlySvgasimgSupport=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","svgasimg"));
/*!
 * add touch support class
 */
var earlyHasTouch;(function(a,b){var c="ontouchstart" in document.documentElement?b:"no-"+b;(earlyHasTouch=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","touch"));
/*!
 * return date in YYYY-MM-DD format
 */
var earlyFnGetYyyymmdd=function(){"use strict";var a=new Date,b=a.getDate(),c=a.getMonth(),c=c+1,d=a.getFullYear();10>b&&(b="0"+b);10>c&&(c="0"+c);return d+"-"+c+"-"+b;};
/*!
 * Escape strings for use as JavaScript string literals
 * gist.github.com/englishextra/3053a4dc18c2de3c80ce7d26207681e0
 * modified github.com/joliss/js-string-escape
 */
var jsStringEscape=function(s){return(""+s).replace(/["'\\\n\r\u2028\u2029]/g,function(a){switch(a){case '"':case "'":case "\\":return"\\"+a;case "\n":return"\\n";case "\r":return"\\r";case "\u2028":return"\\u2028";case "\u2029":return"\\u2029"}})};
/*!
 * append details to title
 */
var initialDocumentTitle=document.title||"",
userBrowsingDetails=" ["+(earlyFnGetYyyymmdd()?earlyFnGetYyyymmdd():"")+(earlyDeviceType?" "+earlyDeviceType:"")+(earlyDeviceSize?" "+earlyDeviceSize:"")+(earlyDeviceOrientation?" "+earlyDeviceOrientation:"")+(earlySvgSupport?" "+earlySvgSupport:"")+(earlySvgasimgSupport?" "+earlySvgasimgSupport:"")+(earlyHasTouch?" "+earlyHasTouch:"")+"]";
document.title&&(document.title=jsStringEscape(document.title+userBrowsingDetails));
/*!
 * modified JavaScript Sync/Async forEach - v0.1.2 - 1/10/2012
 * github.com/cowboy/javascript-sync-async-foreach
 * Copyright (c) 2012 "Cowboy" Ben Alman; Licensed MIT
 * removed Node.js / browser support wrapper function
 * forEach(a,function(e){console.log("eachCallback: "+e);},!1});
 * forEach(a,function(e){console.log("eachCallback: "+e);},function(){console.log("doneCallback");});
 */
var forEach=function(a,b,c){var d=-1,e=a.length>>>0;(function f(g){var h,j=g===!1;do++d;while(!(d in a)&&d!==e);if(j||d===e){c&&c(!j,a);return}g=b.call({async:function(){return h=!0,f}},a[d],d,a),h||f(g)})()};
/*!
 * A function for elements selection
 * 0.1.9
 * github.com/finom/bala
 * global $ renamed to BALA, prepended var
 * a = BALA.one("#someid");
 * a = BALA.one(".someclass");
 * a = BALA(".someclass");
 */
var BALA=function(e,f,g){function c(a,b,d){d=Object.create(c.fn);a&&d.push.apply(d,a[f]?[a]:""+a===a?/</.test(a)?((b=e.createElement(b||f)).innerHTML=a,b.children):b?(b=c(b)[0])?b[g](a):d:e[g](a):"function"==typeof a?e.readyState[7]?a():e[f]("DOMContentLoaded",a):a);return d}c.fn=[];c.one=function(a,b){return c(a,b)[0]||null};return c}(document,"addEventListener","querySelectorAll");
/*!
 * modified for babel crel - a small, simple, and fast DOM creation utility
 * github.com/KoryNunn/crel
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * crel(tagName/dom element [, attributes, child1, child2, childN...])
 * var element=crel('div',crel('h1','Crello World!'),crel('p','This is crel'),crel('input',{type:'number'}));
 */
var crel=(function(){function e(){var o,a=arguments,p=a[0],m=a[1],x=2,v=a.length,b=e[f];if(p=e[c](p)?p:d.createElement(p),1===v)return p;if((!l(m,t)||e[u](m)||s(m))&&(--x,m=null),v-x===1&&l(a[x],"string")&&void 0!==p[r])p[r]=a[x];else for(;v>x;++x)if(o=a[x],null!=o)if(s(o))for(var g=0;g<o.length;++g)y(p,o[g]);else y(p,o);for(var h in m)if(b[h]){var N=b[h];typeof N===n?N(p,m[h]):p[i](N,m[h])}else p[i](h,m[h]);return p}var n="function",t="object",o="nodeType",r="textContent",i="setAttribute",f="attrMap",u="isNode",c="isElement",d=typeof document===t?document:{},l=function(e,n){return typeof e===n},a=typeof Node===n?function(e){return e instanceof Node}:function(e){return e&&l(e,t)&&o in e&&l(e.ownerDocument,t)},p=function(n){return e[u](n)&&1===n[o]},s=function(e){return e instanceof Array},y=function(n,t){e[u](t)||(t=d.createTextNode(t)),n.appendChild(t)};return e[f]={},e[c]=p,e[u]=a,"undefined"!=typeof Proxy&&(e.proxy=new Proxy(e,{get:function(n,t){return!(t in e)&&(e[t]=e.bind(null,t)),e[t]}})),e}());
/*!
 * modified for babel Zenscroll 3.2.2
 * github.com/zengabor/zenscroll
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * Copyright 2015–2016 Gabor Lenard
 * minified with closure-compiler.appspot.com/home
 * github.com/zengabor/zenscroll/blob/dist/zenscroll.js
 */
var zenscroll=(function(){"use strict";if("undefined"==typeof window||!("document"in window)){return{};}var t=function(t,e,n){e=e||999,n||0===n||(n=9);var o,i=document.documentElement,r=function(){return"getComputedStyle"in window&&"smooth"===window.getComputedStyle(t?t:document.body)["scroll-behavior"]},c=function(){return t?t.scrollTop:window.scrollY||i.scrollTop},u=function(){return t?Math.min(t.offsetHeight,window.innerHeight):window.innerHeight||i.clientHeight},f=function(e){return t?e.offsetTop:e.getBoundingClientRect().top+c()-i.offsetTop},l=function(){clearTimeout(o),o=0},a=function(n,f,a){if(l(),r())(t||window).scrollTo(0,n),a&&a();else{var d=c(),s=Math.max(n,0)-d;f=f||Math.min(Math.abs(s),e);var m=(new Date).getTime();!function e(){o=setTimeout(function(){var n=Math.min(((new Date).getTime()-m)/f,1),o=Math.max(Math.floor(d+s*(n<.5?2*n*n:n*(4-2*n)-1)),0);t?t.scrollTop=o:window.scrollTo(0,o),n<1&&u()+o<(t||i).scrollHeight?e():(setTimeout(l,99),a&&a())},9)}()}},d=function(t,e,o){a(f(t)-n,e,o)},s=function(t,e,o){var i=t.getBoundingClientRect().height,r=f(t),l=r+i,s=u(),m=c(),h=m+s;r-n<m||i+n>s?d(t,e,o):l+n>h?a(l-s+n,e,o):o&&o()},m=function(t,e,n,o){a(Math.max(f(t)-u()/2+(n||t.getBoundingClientRect().height/2),0),e,o)},h=function(t,o){t&&(e=t),(0===o||o)&&(n=o)};return{setup:h,to:d,toY:a,intoView:s,center:m,stop:l,moving:function(){return!!o}}},e=t();if("addEventListener"in window&&"smooth"!==document.body.style.scrollBehavior&&!window.noZensmooth){var n=function(t){try{history.replaceState({},"",window.location.href.split("#")[0]+t)}catch(t){}};window.addEventListener("click",function(t){for(var o=t.target;o&&"A"!==o.tagName;)o=o.parentNode;if(!(!o||1!==t.which||t.shiftKey||t.metaKey||t.ctrlKey||t.altKey)){var i=o.getAttribute("href")||"";if(0===i.indexOf("#"))if("#"===i)t.preventDefault(),e.toY(0),n("");else{var r=o.hash.substring(1),c=document.getElementById(r);c&&(t.preventDefault(),e.to(c),n("#"+r))}}},!1)}return{createScroller:t,setup:e.setup,to:e.to,toY:e.toY,intoView:e.intoView,center:e.center,stop:e.stop,moving:e.moving};}());
/*!
 * Scroll to top with Zenscroll and fallback
 */
var scrollToTop=function(){var w=window,g=function(){w.zenscroll?zenscroll.toY(0):w.scrollTo(0,0);};w.setImmediate?setImmediate(function(){g()}):setTimeout(function(){g();});};
/*!
 * Plain javascript replacement for jQuery's .ready()
 * so code can be scheduled to run when the document is ready
 * github.com/jfriend00/docReady
 * docReady(function(){});
 * simple substitute by Christoph at stackoverflow.com/questions/8100576/how-to-check-if-dom-is-ready-without-a-framework
 * (function(){var a=document.readyState;"interactive"===a||"complete"===a?(function(){}()):setTimeout(arguments.callee,100)})();
 */
(function(funcName,baseObj){"use strict";funcName=funcName||"docReady";baseObj=baseObj||window;var readyList=[];var readyFired=false;var readyEventHandlersInstalled=false;function ready(){if(!readyFired){readyFired=true;for(var i=0;i<readyList.length;i++){readyList[i].fn.call(window,readyList[i].ctx);}readyList=[];}}function readyStateChange(){if(document.readyState==="complete"){ready();}}baseObj[funcName]=function(callback,context){if(readyFired){setTimeout(function(){callback(context);},1);return;}else{readyList.push({fn:callback,ctx:context});}if(document.readyState==="complete"||(!document.attachEvent&&document.readyState==="interactive")){setTimeout(ready,1);}else if(!readyEventHandlersInstalled){if(document.addEventListener){document.addEventListener("DOMContentLoaded",ready,false);window.addEventListener("load",ready,false);}else{document.attachEvent("onreadystatechange",readyStateChange);window.attachEvent("onload",ready);}readyEventHandlersInstalled=true;}}})("docReady",window);
/*!
 * modified for babel Evento - v1.0.0
 * by Erik Royall <erikroyalL@hotmail.com> (http://erikroyall.github.io)
 * changed this or window to self as argument
 * added if("undefined"==typeof window||!("document"in window))
 * {return console.log("window is undefined or document is not in window"),!1;}
 * Dual licensed under MIT and GPL
 * Array.prototype.indexOf shim
 * developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
 * gist.github.com/erikroyall/6618740
 * gist.github.com/englishextra/3a959e4da0fcc268b140
 * evento.add(window,"load",function(){});
 */
if(!Array.prototype.indexOf){Array.prototype.indexOf=function(searchElement){'use strict';if(this==null){throw new TypeError();}var n,k,t=Object(this),len=t.length>>>0;if(len===0){return-1;}n=0;if(arguments.length>1){n=Number(arguments[1]);if(n!=n){n=0;}else if(n!=0&&n!=Infinity&&n!=-Infinity){n=(n>0||-1)*Math.floor(Math.abs(n));}}if(n>=len){return-1;}for(k=n>=0?n:Math.max(len-Math.abs(n),0);k<len;k++){if(k in t&&t[k]===searchElement){return k;}}return-1;};}var evento=(function(){if("undefined"==typeof window||!("document"in window)){return console.log("window is undefined or document is not in window"),!1;}var win=window,doc=win.document,_handlers={},addEvent,removeEvent,triggerEvent;addEvent=(function(){if(typeof doc.addEventListener==="function"){return function(el,evt,fn){el.addEventListener(evt,fn,false);_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}else if(typeof doc.attachEvent==="function"){return function(el,evt,fn){el.attachEvent(evt,fn);_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}else{return function(el,evt,fn){el["on"+evt]=fn;_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}}());removeEvent=(function(){if(typeof doc.removeEventListener==="function"){return function(el,evt,fn){el.removeEventListener(evt,fn,false);Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}else if(typeof doc.detachEvent==="function"){return function(el,evt,fn){el.detachEvent(evt,fn);Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}else{return function(el,evt,fn){el["on"+evt]=undefined;Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}}());triggerEvent=function(el,evt){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];for(var _i=0,_l=_handlers[el][evt].length;_i<_l;_i+=1){_handlers[el][evt][_i]();}};return{add:addEvent,remove:removeEvent,trigger:triggerEvent,_handlers:_handlers};}());
/*!
 * Waypoints - 4.0.0
 * Copyright © 2011-2015 Caleb Troughton
 * Licensed under the MIT license.
 * github.com/imakewebthings/waypoints/blog/master/licenses.txt
 */
!function(){"use strict";function t(n){if(!n)throw new Error("No options passed to Waypoint constructor");if(!n.element)throw new Error("No element option passed to Waypoint constructor");if(!n.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,n),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=n.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var n in i)e.push(i[n]);for(var o=0,r=e.length;r>o;o++)e[o][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.invokeAll("enable")},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=o.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,n[t.waypointContextKey]=this,i+=1,this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,n={},o=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical);t&&e&&(this.adapter.off(".waypoints"),delete n[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,o.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||o.isTouch)&&(e.didScroll=!0,o.requestAnimationFrame(t))})},e.prototype.handleResize=function(){o.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var n=e[i],o=n.newScroll>n.oldScroll,r=o?n.forward:n.backward;for(var s in this.waypoints[i]){var l=this.waypoints[i][s],a=n.oldScroll<l.triggerPoint,h=n.newScroll>=l.triggerPoint,p=a&&h,u=!a&&!h;(p||u)&&(l.queueTrigger(r),t[l.group.id]=l.group)}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?o.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?o.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var n=0,o=t.length;o>n;n++)t[n].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),n={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var l in this.waypoints[r]){var a,h,p,u,c,f=this.waypoints[r][l],d=f.options.offset,y=f.triggerPoint,g=0,w=null==y;f.element!==f.element.window&&(g=f.adapter.offset()[s.offsetProp]),"function"==typeof d?d=d.apply(f):"string"==typeof d&&(d=parseFloat(d),f.options.offset.indexOf("%")>-1&&(d=Math.ceil(s.contextDimension*d/100))),a=s.contextScroll-s.contextOffset,f.triggerPoint=g+a-d,h=y<s.oldScroll,p=f.triggerPoint>=s.oldScroll,u=h&&p,c=!h&&!p,!w&&u?(f.queueTrigger(s.backward),n[f.group.id]=f.group):!w&&c?(f.queueTrigger(s.forward),n[f.group.id]=f.group):w&&s.oldScroll>=f.triggerPoint&&(f.queueTrigger(s.forward),n[f.group.id]=f.group)}}return o.requestAnimationFrame(function(){for(var t in n)n[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in n)n[t].refresh()},e.findByElement=function(t){return n[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},o.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},o.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),n[this.axis][this.name]=this}var n={vertical:{},horizontal:{}},o=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var n=this.triggerQueues[i],o="up"===i||"left"===i;n.sort(o?e:t);for(var r=0,s=n.length;s>r;r+=1){var l=n[r];(l.options.continuous||r===n.length-1)&&l.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=o.Adapter.inArray(e,this.waypoints),n=i===this.waypoints.length-1;return n?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=o.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=o.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return n[t.axis][t.name]||new i(t)},o.Group=i}(),function(){"use strict";function t(t){return t===t.window}function e(e){return t(e)?e:e.defaultView}function i(t){this.element=t,this.handlers={}}var n=window.Waypoint;i.prototype.innerHeight=function(){var e=t(this.element);return e?this.element.innerHeight:this.element.clientHeight},i.prototype.innerWidth=function(){var e=t(this.element);return e?this.element.innerWidth:this.element.clientWidth},i.prototype.off=function(t,e){function i(t,e,i){for(var n=0,o=e.length-1;o>n;n++){var r=e[n];i&&i!==r||t.removeEventListener(r)}}var n=t.split("."),o=n[0],r=n[1],s=this.element;if(r&&this.handlers[r]&&o)i(s,this.handlers[r][o],e),this.handlers[r][o]=[];else if(o)for(var l in this.handlers)i(s,this.handlers[l][o]||[],e),this.handlers[l][o]=[];else if(r&&this.handlers[r]){for(var a in this.handlers[r])i(s,this.handlers[r][a],e);this.handlers[r]={}}},i.prototype.offset=function(){if(!this.element.ownerDocument)return null;var t=this.element.ownerDocument.documentElement,i=e(this.element.ownerDocument),n={top:0,left:0};return this.element.getBoundingClientRect&&(n=this.element.getBoundingClientRect()),{top:n.top+i.pageYOffset-t.clientTop,left:n.left+i.pageXOffset-t.clientLeft}},i.prototype.on=function(t,e){var i=t.split("."),n=i[0],o=i[1]||"__default",r=this.handlers[o]=this.handlers[o]||{},s=r[n]=r[n]||[];s.push(e),this.element.addEventListener(n,e)},i.prototype.outerHeight=function(e){var i,n=this.innerHeight();return e&&!t(this.element)&&(i=window.getComputedStyle(this.element),n+=parseInt(i.marginTop,10),n+=parseInt(i.marginBottom,10)),n},i.prototype.outerWidth=function(e){var i,n=this.innerWidth();return e&&!t(this.element)&&(i=window.getComputedStyle(this.element),n+=parseInt(i.marginLeft,10),n+=parseInt(i.marginRight,10)),n},i.prototype.scrollLeft=function(){var t=e(this.element);return t?t.pageXOffset:this.element.scrollLeft},i.prototype.scrollTop=function(){var t=e(this.element);return t?t.pageYOffset:this.element.scrollTop},i.extend=function(){function t(t,e){if("object"==typeof t&&"object"==typeof e)for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i]);return t}for(var e=Array.prototype.slice.call(arguments),i=1,n=e.length;n>i;i++)t(e[0],e[i]);return e[0]},i.inArray=function(t,e,i){return null==e?-1:e.indexOf(t,i)},i.isEmptyObject=function(t){for(var e in t)return!1;return!0},n.adapters.push({name:"noframework",Adapter:i}),n.Adapter=i}();
/*!
 * Lazyload images, iframes, widgets with a standalone JavaScript lazyloader
 * v3.2.2
 * https://vvo.github.io/lazyload/
 * github.com/englishextra/lazyload
 */
(function(u){var l;"undefined"!==typeof window?l=window:"undefined"!==typeof self&&(l=self);l.lazyload=u()})(function(){return function l(n,k,e){function r(g,m){if(!k[g]){if(!n[g]){var f="function"==typeof require&&require;if(!m&&f)return f(g,!0);if(t)return t(g,!0);f=Error("Cannot find module '"+g+"'");throw f.code="MODULE_NOT_FOUND",f;}f=k[g]={exports:{}};n[g][0].call(f.exports,function(e){var c=n[g][1][e];return r(c?c:e)},f,f.exports,l,n,k,e)}return k[g].exports}for(var t="function"==typeof require&&require,m=0;m<e.length;m++)r(e[m]);return r}({1:[function(l,n,k){(function(e){function r(c){-1===k.call(p,c)&&p.push(c)}function t(c){function b(b){var d;d="function"===typeof c.src?c.src(b):b.getAttribute(c.src);d&&(b.src=d);b.setAttribute("data-lzled",!0);a[k.call(a,b)]=null}c=g({offset:333,src:"data-src",container:!1},c||{});"string"===typeof c.src&&r(c.src);var a=[];return function(h){h.onload=null;h.removeAttribute("onload");h.onerror=null;h.removeAttribute("onerror");-1===k.call(a,h)&&f(h,c,b)}}function m(c){c="HTML"+c+"Element";if(!1!==c in e){var b=e[c].prototype.getAttribute;e[c].prototype.getAttribute=function(a){if("src"===a){for(var h,d=0,c=p.length;d<c&&!(h=b.call(this,p[d]));d++);return h||b.call(this,a)}return b.call(this,a)}}}function g(c,b){for(var a in c)void 0===b[a]&&(b[a]=c[a]);return b}function k(c){for(var b=this.length;b--&&this[b]!==c;);return b}n.exports=t;var f=l("in-viewport"),p=["data-src"];e.lzld=t();m("Image");m("IFrame")}).call(this,"undefined"!==typeof global?global:"undefined"!==typeof self?self:"undefined"!==typeof window?window:{})},{"in-viewport":2}],2:[function(l,n,k){(function(e){function k(b,a,h){b.attachEvent?b.attachEvent("on"+a,h):b.addEventListener(a,h,!1)}function l(b,a,h){var d;return function(){var c=this,e=arguments,w=h&&!d;clearTimeout(d);d=setTimeout(function(){d=null;h||b.apply(c,e)},a);w&&b.apply(c,e)}}function m(b){function a(b,a,c){return{watch:function(){d.add(b,a,c)},dispose:function(){d.remove(b)}}}function h(a,d){if(!(c(e.document.documentElement,a)&&c(e.document.documentElement,b)&&a.offsetWidth&&a.offsetHeight))return!1;var h=a.getBoundingClientRect(),f,q,k,g;b===e.document.body?(f=-d,q=-d,k=e.document.documentElement.clientWidth+d,g=e.document.documentElement.clientHeight+d):(g=b.getBoundingClientRect(),f=g.top-d,q=g.left-d,k=g.right+d,g=g.bottom+d);return h.right>=q&&h.left<=k&&h.bottom>=f&&h.top<=g}var d=g(),q=b===e.document.body?e:b,f=l(d.checkAll(function(b,a,c){h(b,a)&&(d.remove(b),c(b))}),15);k(q,"scroll",f);q===e&&k(e,"resize",f);p&&v(d,b,f);setInterval(f,150);return{container:b,isInViewport:function(b,d,c){if(!c)return h(b,d);b=a(b,d,c);b.watch();return b}}}function g(){function b(b){for(var a=c.length-1;0<=a;a--)if(c[a][0]===b)return a;return-1}function a(a){return-1!==b(a)}var c=[];return{add:function(b,e,f){a(b)||c.push([b,e,f])},remove:function(a){a=b(a);-1!==a&&c.splice(a,1)},isWatched:a,checkAll:function(b){return function(){for(var a=c.length-1;0<=a;a--)b.apply(this,c[a])}}}}function v(b,a,c){function d(a){a=g.call([],Array.prototype.slice.call(a.addedNodes),a.target);return 0<f.call(a,b.isWatched).length}var e=new MutationObserver(function(a){!0===a.some(d)&&setTimeout(c,0)}),f=Array.prototype.filter,g=Array.prototype.concat;e.observe(a,{childList:!0,subtree:!0,attributes:!0})}n.exports=function(b,a,c){var d=e.document.body;if(void 0===a||"function"===typeof a)c=a,a={};d=a.container||d;a=a.offset||0;for(var g=0;g<f.length;g++)if(f[g].container===d)return f[g].isInViewport(b,a,c);return f[f.push(m(d))-1].isInViewport(b,a,c)};var f=[],p="function"===typeof e.MutationObserver,c=e.document.documentElement.compareDocumentPosition?function(b,a){return!!(b.compareDocumentPosition(a)&16)}:e.document.documentElement.contains?function(b,a){return b!==a&&(b.contains?b.contains(a):!1)}:function(b,a){for(;a=a.parentNode;)if(a===b)return!0;return!1}}).call(this,"undefined"!==typeof global?global:"undefined"!==typeof self?self:"undefined"!==typeof window?window:{})},{}]},{},[1])(1)});
/*!
 * How can I check if a CSS file has been included already?
 * stackoverflow.com/questions/18155347/how-can-i-check-if-a-js-file-has-been-included-already
 */
var styleIsLoaded=function(h){var a=document.styleSheets||"";if(a)for(var b=0,d=a.length;b<d;b++)if(a[b].href==h)return!0;return!1};
/*!
 * How can I check if a JS file has been included already?
 * stackoverflow.com/questions/18155347/how-can-i-check-if-a-js-file-has-been-included-already
 */
var scriptIsLoaded=function(s){for(var b=document.getElementsByTagName("script")||"",a=0;a<b.length;a++)if(b[a].getAttribute("src")==s)return!0;return!1};
/*!
 * Load and execute JS via AJAX
 * gist.github.com/englishextra/8dc9fe7b6ff8bdf5f9b483bf772b9e1c
 * IE 5.5+, Firefox, Opera, Chrome, Safari XHR object
 * gist.github.com/Xeoncross/7663273
 * modified callback(x.responseText,x); to callback(eval(x.responseText),x);
 * stackoverflow.com/questions/3728798/running-javascript-downloaded-with-xmlhttprequest
 */
var AJAXLoadAndTriggerJs=function(u,cb,d){var w=window;try{var x=w.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");x.open(d?"POST":"GET",u,!0);x.setRequestHeader("X-Requested-With","XMLHttpRequest");x.setRequestHeader("Content-type","application/x-www-form-urlencoded");x.onreadystatechange=function(){if(x.readyState>3){if(x.responseText){eval(x.responseText);cb&&"function"===typeof cb&&cb(x.responseText);}}};x.send(d);}catch(e){console.log("Error XMLHttpRequest-ing file",e);}};
/*!
 * remove all children of parent element
 */
var removeChildElements=function(a){if(a)for(;a.firstChild;)a.removeChild(a.firstChild);};
/*!
 * set style display block of an element
 */
var setStyleDisplayBlock=function(a){a&&(a.style.display="block");};
/*!
 * set style display none of an element
 */
var setStyleDisplayNone=function(a){a&&(a.style.display="none");};
/*!
 * toggle style display of an element
 */
var toggleStyleDisplay=function(a,show,hide){if(a){a.style.display=hide==a.style.display||""==a.style.display?show:hide;}};
/*!
 * set style opacity of an element
 */
var setStyleOpacity=function(a,n){a&&(a.style.opacity=n);};
/*!
 * set style visibility visible of an element
 */
var setStyleVisibilityVisible=function(a){a&&(a.style.visibility="visible");};
/*!
 * set style visibility hidden of an element
 */
var setStyleVisibilityHidden=function(a){a&&(a.style.visibility="hidden");};
/*!
 * change location
 */
var changeDocumentLocation=function(h){h&&(document.location.href=h);};
/*!
 * get http or https
 */
var getHTTPProtocol=function(){var a=window.location.protocol||"";return"http:"===a?"http":"https:"===a?"https":""};
/*!
 * has http
 */
var hasHTTP=function(url){return/^(http|https):\/\//i.test(url)?!0:!1};
/*!
 * get http or https
 */
var getHTTPProtocol=function(){var a=window.location.protocol||"";return"http:"===a?"http":"https:"===a?"https":""};
/*!
 * show data loading spinner
 */
var showDataLoadingSpinner = function () {
	var h = BALA.one("html") || "",
	data_loading = "data-loading";
	h && h.classList.add(data_loading);
};
/*!
 * hide data loading spinner
 */
var hideDataLoadingSpinner = function () {
	var h = BALA.one("html") || "",
	data_loading = "data-loading";
	if (h) {
		setAndClearTimeout(function () {
			h.classList.remove(data_loading);
		}, 500);
	}
};
/*!
 * Open external links in default browser out of Electron / nwjs
 * gist.github.com/englishextra/b9a8140e1c1b8aa01772375aeacbf49b
 * stackoverflow.com/questions/32402327/how-can-i-force-external-links-from-browser-window-to-open-in-a-default-browser
 * github.com/nwjs/nw.js/wiki/shell
 * electron - file: | nwjs - chrome-extension: | http: Intel XDK
 */
var openDeviceBrowser = function (a) {
	var w = window,
	g = function () {
		var electronShell = require("electron").shell;
		electronShell.openExternal(a);
	},
	k = function () {
		var nwGui = require("nw.gui");
		nwGui.Shell.openExternal(a);
	},
	q = function () {
		var win = w.open(a, "_blank");
		win.focus();
	},
	v = function () {
		w.open(a, "_system", "scrollbars=1,location=no");
	};
	if (!!isElectron) {
		g();
	} else if (!!isNwjs) {
		k();
	} else {
		if (!!getHTTPProtocol()) {
			q();
		} else {
			v();
		}
	}
};
/*!
 * set target blank to external links
 */
var setTargetBlankOnAnchors = function () {
	var w = window,
	a = BALA("a") || "",
	g = function (e) {
		var p = e.getAttribute("href") || "";
		if (p
			&& (/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)|(localhost)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/.test(p) || /http:\/\/localhost/.test(p))
			&& !e.getAttribute("rel")) {
			crel(e, {
				"title" : "\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0432\u043d\u0435\u0448\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441 " + (p.match(/:\/\/(.[^/]+)/)[1] || "") + " \u043e\u0442\u043a\u0440\u043e\u0435\u0442\u0441\u044f \u0432 \u043d\u043e\u0432\u043e\u0439 \u0432\u043a\u043b\u0430\u0434\u043a\u0435 \u0438\u043b\u0438 \u043e\u043a\u043d\u0435"
			});
			if (w.openDeviceBrowser) {
				crel(e, {
					"onclick" : "openDeviceBrowser('" + jsStringEscape(p) + "');return !1;"
				});
			} else {
				crel(e, {
					"target" : "_blank"
				});
			}
		}
	},
	k = function () {
			if (w._) {
				_.each(a, function (e) {
					g(e);
				});
			} else if (w.forEach) {
				forEach(a, function (e) {
					g(e);
				}, !1);
			} else {
				for (var i = 0, l = a.length; i < l; i += 1) {
					g(a[i]);
				};
			}
	};
	if (a) {
		k();
	}
};
docReady(function () {
	setTargetBlankOnAnchors();
});
/*!
 * init fastclick
 * github.com/ftlabs/fastclick
 */
var initFastclick = function () {
	var w = window,
	b = BALA.one("body") || "",
	fastclick_js_src = "/cdn/fastclick/1.0.6/js/fastclick.fixed.min.js",
	g = function () {
		AJAXLoadAndTriggerJs(fastclick_js_src, function () {
			w.FastClick && FastClick.attach(b);
		});
	};
	if (!!getHTTPProtocol()) {
		g();
	}
};
"undefined" !== typeof earlyHasTouch && "touch" === earlyHasTouch && evento.add(window, "load", function () {
	initFastclick();
});
/*!
 * init all Masonry grids
 */
var initAllMasonryGrids = function () {
	var w = window,
	g = ".grid",
	h = ".grid-item",
	k = ".grid-sizer",
	grid = BALA.one(g) || "",
	grid_item = BALA.one(h) || "",
	masonry_js_src = "../../cdn/masonry/4.0.0/js/masonry.pkgd.fixed.min.js",
	q = function (grid, h, k) {
		msnry = new Masonry(grid, {
				itemSelector : h,
				columnWidth : k,
				gutter : 0
			});
	},
	v = function (a) {
		if (w._) {
			_.each(a, function (e) {
				q(e, h, k);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				q(e, h, k);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				q(a[i], h, k);
			};
		}
	};
	if (!!getHTTPProtocol() && grid && grid_item) {
		var a = BALA(g);
		if (a) {
				scriptIsLoaded(masonry_js_src) || loadJS(masonry_js_src, function () {
					if (w.Masonry) {
					var msnry;
					v(a);
				}
			});
		}
	}
};
evento.add(window, "load", function () {
	initAllMasonryGrids();
});
/*!
 * init all Packery grids
 */
/* var initAllPackeryGrids = function () {
	var w = window,
	g = ".grid",
	h = ".grid-item",
	k = ".grid-sizer",
	grid = BALA.one(g) || "",
	grid_item = BALA.one(h) || "",
	packery_js_src = "../../cdn/packery/2.0.0/js/packery.pkgd.fixed.min.js",
	draggabilly_js_src = "../../cdn/draggabilly/2.1.0/js/draggabilly.pkgd.fixed.min.js",
	q = function (grid, h, k) {
		pckry = new Packery(grid, {
				itemSelector : h,
				columnWidth : k,
				gutter : 0
			});
	},
	v = function (a) {
		if (w._) {
			_.each(a, function (e) {
				q(e, h, k);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				q(e, h, k);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				q(a[i], h, k);
			};
		}
	},
	z = function (a) {
		var s = function (e) {
			var draggableElem = e;
			draggie = new Draggabilly(draggableElem, {});
			draggies.push(draggie);
		};
		var draggies = [];
		if (w._) {
			_.each(a, function (e) {
				s(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				s(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				s(a[i]);
			};
		}
		pckry && pckry.bindDraggabillyEvents(draggie);
	};
	if (!!getHTTPProtocol() && grid && grid_item) {
		var a = BALA(g);
		if (a) {
				scriptIsLoaded(packery_js_src) || loadJS(packery_js_src, function () {
					if (w.Packery) {
					var pckry;
					v(a);
					var c = BALA(h) || "";
					if (c) {
						scriptIsLoaded(draggabilly_js_src) || loadJS(draggabilly_js_src, function () {
							if (w.Draggabilly) {
								var draggie;
								z(c);
							}
						});
					}
				}
			});
		}
	}
};
evento.add(window, "load", function () {
	initAllPackeryGrids();
}); */
/*!
 * init photoswipe
 */
var initPhotoswipe = function () {
	var w = window,
	c = ".article-gallery",
	gallery = BALA.one(c) || "",
	photoswipe_css_href = "../../cdn/photoswipe/4.1.0/custom/css/photoswipe.bundle.min.css",
	photoswipe_js_src = "../../cdn/photoswipe/4.1.0/custom/js/photoswipe.bundle.min.js";
	if (gallery) {
		var galleries = BALA(".article-gallery"),
		pswp = function(k){var p=function(l){for(var a=l.childNodes,b=a.length,e=[],c,g,d,f=0;f<b;f++)if(l=a[f],1===l.nodeType){c=l.children;g=l.getAttribute("data-size").split("x");d={src:l.getAttribute("href"),w:parseInt(g[0],10),h:parseInt(g[1],10),author:l.getAttribute("data-author")};d.el=l;0<c.length&&(d.msrc=c[0].getAttribute("src"),1<c.length&&(d.title=c[1].innerHTML));if(c=l.getAttribute("data-med"))g=l.getAttribute("data-med-size").split("x"),d.m={src:c,w:parseInt(g[0],10),h:parseInt(g[1], 10)};d.o={src:d.src,w:d.w,h:d.h};e.push(d)}return e},q=function a(b,e){return b&&(e(b)?b:a(b.parentNode,e))},h=function(a){a=a||window.event;a.preventDefault?a.preventDefault():a.returnValue=!1;if(a=q(a.target||a.srcElement,function(a){return"A"===a.tagName})){for(var b=a.parentNode,e=a.parentNode.childNodes,c=e.length,g=0,d,f=0;f<c;f++)if(1===e[f].nodeType){if(e[f]===a){d=g;break}g++}0<=d&&n(d,b);return!1}},n=function(a,b,e){e=document.querySelectorAll(".pswp")[0];var c,g;g=p(b);a={index:a,galleryUID:b.getAttribute("data-pswp-uid"), getThumbBoundsFn:function(a){var b=window.pageYOffset||document.documentElement.scrollTop;a=g[a].el.children[0].getBoundingClientRect();return{x:a.left,y:a.top+b,w:a.width}},addCaptionHTMLFn:function(a,b,c){if(!a.title)return b.children[0].innerText="",!1;b.children[0].innerHTML=a.title+"<br/><small>Photo: "+a.author+"</small>";return!0}};c=new PhotoSwipe(e,PhotoSwipeUI_Default,g,a);var d,f=!1,k=!0,h;c.listen("beforeResize",function(){var a=window.devicePixelRatio?window.devicePixelRatio:1,a=Math.min(a, 2.5);d=c.viewportSize.x*a;1200<=d||!c.likelyTouchDevice&&800<d||1200<screen.width?f||(h=f=!0):f&&(f=!1,h=!0);h&&!k&&c.invalidateCurrItems();k&&(k=!1);h=!1});c.listen("gettingData",function(a,b){f?(b.src=b.o.src,b.w=b.o.w,b.h=b.o.h):(b.src=b.m.src,b.w=b.m.w,b.h=b.m.h)});c.init()};k=document.querySelectorAll(k);for(var m=0,r=k.length;m<r;m++)k[m].setAttribute("data-pswp-uid",m+1),k[m].onclick=h;h=function(){var a=window.location.hash.substring(1),b={};if(5>a.length)return b;for(var a=a.split("&"),e= 0;e<a.length;e++)if(a[e]){var c=a[e].split("=");2>c.length||(b[c[0]]=c[1])}b.gid&&(b.gid=parseInt(b.gid,10));if(!b.hasOwnProperty("pid"))return b;b.pid=parseInt(b.pid,10);return b}();0<h.pid&&0<h.gid&&n(h.pid-1,k[h.gid-1],!0)};
	fixed_protocol = getHTTPProtocol() ? getHTTPProtocol() : "http",
	g = function (e) {
		var m = e.dataset.med || "";
		if (m && !hasHTTP(m) && /^\/\//.test(m)) {
			e.dataset.med = m.replace (/^/, fixed_protocol + ":");
		}
		/*!
		 * dont use href to read href, use getAttribute, because href adds protocol
		 */
		var h = e.getAttribute("href");
		if (h && !hasHTTP(h) && /^\/\//.test(h)) {
			e.href = h.replace (/^/, fixed_protocol + ":");
		}
	},
	k = function (a) {
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	},
	q = function (e) {
		setStyleDisplayBlock(e);
		var a = BALA("a", e) || "";
		if (a) {
			k(a);
		}
	},
	v = function () {
		var galleries = BALA(c);
		if (w._) {
			_.each(galleries, function (e) {
				q(e);
			});
		} else if (w.forEach) {
			forEach(galleries, function (e) {
				q(e);
			}, !1);
		} else {
			for (var i = 0, l = galleries.length; i < l; i += 1) {
				q(galleries[i]);
			};
		}
	},
	z = function () {
		styleIsLoaded(photoswipe_css_href) || loadCSS(photoswipe_css_href, function () {
			scriptIsLoaded(photoswipe_js_src) || loadJS(photoswipe_js_src, function () {
				v();
				pswp(c);
			});
		});
	};
	if (gallery) {
		z();
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	initPhotoswipe();
});
/*!
 * init tablesort
 * github.com/tristen/tablesort
 */
var initTablesort = function () {
	var w = window,
	c = "table.sort",
	table = BALA.one(c) || "",
	tablesort_css_href = "../../cdn/tablesort/4.0.1/custom/css/tablesort.min.css",
	tablesort_js_src = "../../cdn/tablesort/4.0.1/js/tablesort.fixed.min.js",
	g = function (e) {
		var t_id = e.id || "";
		if (t_id) {
			var t = BALA.one("#" + t_id),
			t_caption = BALA.one("#" + t_id + " caption") || t.insertBefore(crel("caption"), t.firstChild),
			tablesort = new Tablesort(t);
			if (t_caption) {
				crel(t_caption, "\u0421\u043E\u0440\u0442\u0438\u0440\u0443\u0435\u043C\u0430\u044F \u0442\u0430\u0431\u043B\u0438\u0446\u0430");
			}
		}
	},
	k = function () {
		var a = BALA(c);
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	};
	if (table) {
		scriptIsLoaded(tablesort_js_src) || loadJS(tablesort_js_src, function () {
			k();
		});
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	initTablesort();
});
/*!
 * init hint.css
 */
var initHintCss = function () {
	var hint = BALA.one('[class*=" hint-"]') || BALA.one('[class^="hint-"]') || "",
	hint_css_href = "../../cdn/hint.css/2.2.0/custom/css/hint.min.css";
	if (hint) {
		styleIsLoaded(hint_css_href) || loadCSS(hint_css_href);
	}
};
evento.add(window, "load", function () {
	initHintCss();
});
/*!
 * replace img src with data-src
 */
var replaceImgSrcWithDataSrc = function () {
	var w = window,
	a = BALA("img") || "",
	g = function (e) {
		var s = e.dataset.src || "",
		f = e.classList.contains("data-src-img"),
		fixed_protocol = getHTTPProtocol() ? getHTTPProtocol() : "http";
		if (s && f) {
			if (!hasHTTP(s) && /^\/\//.test(s)) {
				e.dataset.src = s.replace(/^/, fixed_protocol + ":");
			}
			w.lzld ? lzld(e) : e.src = e.dataset.src;
			setStyleVisibilityVisible(e.parentNode);
			setStyleOpacity(e.parentNode, 1);
		}
	};
	if (a) {
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	}
};
evento.add(window, "load", function () {
	replaceImgSrcWithDataSrc();
});
/*!
 * append media-iframe
 */
var appendMediaIframe = function () {
	var w = window,
	a = BALA("iframe") || "",
	g = function (e) {
		var s = e.dataset.src || "",
		f = e.classList.contains("data-src-iframe"),
		fixed_protocol = getHTTPProtocol() ? getHTTPProtocol() : "http";
		if (s && f) {
			if (!hasHTTP(s) && /^\/\//.test(s)) {
				e.dataset.src = s.replace(/^/, fixed_protocol + ":");
			}
			crel(e, {
				"scrolling" : "no",
				"frameborder" : "no",
				"style" : "border:none;",
				"webkitallowfullscreen" : "true",
				"mozallowfullscreen" : "true",
				"allowfullscreen" : "true"
			});
			w.lzld ? lzld(e) : e.src = e.dataset.src;
			setStyleDisplayBlock(e.parentNode, 1);
		}
	};
	if (a) {
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	}
};
evento.add(window, "load", function () {
	appendMediaIframe();
});
/*!
 * init static select
 */
var initStaticSelect = function () {
	var w = window,
	a = BALA.one("#select") || "",
	g = function (_this) {
		var h = _this.options[_this.selectedIndex].value || "",
		zh = h ? (/^#/.test(h) ? BALA.one(h) : "") : "";
		if (h) {
			w.zenscroll ? (zh ? zenscroll.to(zh) : changeDocumentLocation(h)) : changeDocumentLocation(h);
		}
	},
	k = function () {
		evento.add(a, "change", function () {
			g(this);
		});
	};
	if (a) {
		k();
	}
};
docReady(function () {
	initStaticSelect();
});
/*!
 * show hidden-layer
 */
var showHiddenLayer = function () {
	var w = window,
	h = BALA.one("html") || "",
	c = ".btn-expand-hidden-layer",
	is_active = "is-active",
	btn = BALA.one(c) || "",
	g = function (_this) {
		var s = _this.parentNode.nextElementSibling;
		_this.classList.toggle(is_active),
		s.classList.toggle(is_active);
		return !1;
	},
	k = function (e) {
		evento.add(e, "click", function () {
			g(this);
		});
	},
	q = function () {
		var a = BALA(c);
		if (w._) {
			_.each(a, function (e) {
				k(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				k(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				k(a[i]);
			};
		}
	};
	if (btn) {
		q();
	}
};
docReady(function () {
	showHiddenLayer();
});
/*!
 * init qr-code
 * stackoverflow.com/questions/12777622/how-to-use-enquire-js
 */
var showPageQRRef = function () {
	var w = window,
	d = document,
	a = BALA.one("#qr-code") || "",
	p = w.location.href || "",
	g = function () {
		removeChildElements(a);
		var t = jsStringEscape(d.title ? ("\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u00ab" + d.title.replace(/\[[^\]]*?\]/g, "").trim() + "\u00bb") : ""),
		s = getHTTPProtocol() + "://chart.googleapis.com/chart?cht=qr&chld=M|4&choe=UTF-8&chs=300x300&chl=" + encodeURIComponent(p),
		c = "width:10.000em;height:10.000em;background:transparent;background-size:120.000pt 120.000pt;border:0;vertical-align:bottom;padding:0;margin:0;";
		crel(a,
			crel("img", {
				"src" : s,
				"style" : c,
				"title" : t,
				"alt" : t
			}));
	};
	if (a && p) {
		if (!!getHTTPProtocol()) {
			g();
		} else {
			setStyleDisplayNone(a);
		}
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	showPageQRRef();
});
/*!
 * init nav-menu
 */
var initNavMenu = function () {
	var w = window,
	container = BALA.one("#container") || "",
	page = BALA.one("#page") || "",
	btn = BALA.one("#btn-nav-menu") || "",
	panel = BALA.one("#panel-nav-menu") || "",
	items = BALA("a", panel) || "",
	holder = BALA.one("#holder-panel-menu-more") || "",
	active = "active",
	p = w.location.href || "",
	g = function () {
		if (panel.classList.contains(active)) {
			page.classList.remove(active);
			panel.classList.remove(active);
			btn.classList.remove(active);
		}
	},
	k = function () {
		setStyleDisplayNone(holder);
		page.classList.toggle(active);
		panel.classList.toggle(active);
		btn.classList.toggle(active);
	},
	q = function () {
		setStyleDisplayNone(holder);
		page.classList.remove(active);
		panel.classList.remove(active);
		btn.classList.remove(active);
		/* scrollToTop(); */
	},
	m = function (e) {
		e.classList.remove(active);
	},
	n = function (e) {
		e.classList.add(active);
	},
	s = function (a) {
		if (w._) {
			_.each(a, function (e) {
				m(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				m(e);
			}, !1);
		} else {
			for (var j = 0, l = a.length; j < l; j += 1) {
				m(a[j]);
			};
		}
	},
	v = function (a, e) {
		evento.add(e, "click", function () {
			if (panel.classList.contains(active)) {
				q();
			}
			s(a);
			n(e);
		});
		if (e.href == p) {
			n(e);
		} else {
			m(e);
		}
	},
	z = function () {
		if (w._) {
			_.each(items, function (e) {
				v(items, e);
			});
		} else if (w.forEach) {
			forEach(items, function (e) {
				v(items, e);
			}, !1);
		} else {
			for (var i = 0, l = items.length; i < l; i += 1) {
				v(items, items[i]);
			};
		}
	};
	if (container && page && btn && panel && items) {
		/*!
		 * open or close nav
		 */
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			k();
		}),
		evento.add(container, "click", function () {
			g();
		});
		/*!
		 * close nav, scroll to top, highlight active nav item
		 */
		z();
	}
};
docReady(function () {
	initNavMenu();
});
/*!
 * init menu-more
 */
var initMenuMore = function () {
	var w = window,
	container = BALA.one("#container") || "",
	holder = BALA.one("#holder-panel-menu-more") || "",
	btn = BALA.one("#btn-menu-more") || "",
	panel = BALA.one("#panel-menu-more") || "",
	items = BALA("li", panel) || "",
	g = function (e) {
		evento.add(e, "click", function () {
			setStyleDisplayNone(holder);
		});
	},
	k = function () {
		evento.add(container, "click", function () {
			setStyleDisplayNone(holder);
		});
	},
	q = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			toggleStyleDisplay(holder, "inline-block", "none");
		});
	},
	v = function () {
		if (w._) {
			_.each(items, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(items, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = items.length; i < l; i += 1) {
				g(items[i]);
			};
		}
	};
	if (container && holder && btn && panel && items) {
		/*!
		 * hide menu more on outside click
		 */
		k();
		/*!
		 * show or hide menu more
		 */
		q();
		/*!
		 * hide menu more on item clicked
		 */
		v();
	}
};
docReady(function () {
	initMenuMore();
});
/*!
 * init ui-totop
 */
var initUiTotop = function () {
	var w = window,
	b = BALA.one("body") || "",
	h = BALA.one("html") || "",
	u = "ui-totop",
	v = "ui-totop-hover",
	g = function (cb) {
		crel(b,
			crel("a", {
				"style" : "opacity:0;",
				"href" : "#",
				"title" : "\u041d\u0430\u0432\u0435\u0440\u0445",
				"id" : u,
				"onclick" : "function scrollTop2(c){var b=window.pageYOffset,d=0,e=setInterval(function(b,a){return function(){a-=b*c;window.scrollTo(0,a);d++;(150<d||0>a)&&clearInterval(e)}}(c,b--),50)};window.zenscroll?zenscroll.toY(0):scrollTop2(50);return !1;"
			},
				crel("span", {
					"id" : v
				}), "\u041d\u0430\u0432\u0435\u0440\u0445"));
		!!cb && "function" === typeof cb && cb();
	},
	k = function (_this) {
		var offset = _this.pageYOffset || h.scrollTop || b.scrollTop || "",
		height = _this.innerHeight || h.clientHeight || b.clientHeight || "",
		btn = BALA.one("#" + u) || "";
		if (offset && height && btn) {
			offset > height ? (setStyleVisibilityVisible(btn), setStyleOpacity(btn, 1)) : (setStyleVisibilityHidden(btn), setStyleOpacity(btn, 0));
		}
	},
	q = function () {
		evento.add(w, "scroll", function () {
			k(this);
		});
	};
	if (b) {
		g(function () {
			q();
		});
	}
};
docReady(function () {
	initUiTotop();
});
/*!
 * init pluso-engine or ya-share on click
 */
var showShareOptionsOnClick = function () {
	var pluso = BALA.one(".pluso") || "",
	ya_share2 = BALA.one(".ya-share2") || "",
	btn = BALA.one("#btn-block-social-buttons") || "",
	pluso_like_js_src = getHTTPProtocol() + "://share.pluso.ru/pluso-like.js",
	share_js_src = getHTTPProtocol() + "://yastatic.net/share2/share.js",
	g = function (share_block, btn) {
		setStyleVisibilityVisible(share_block);
		setStyleOpacity(share_block, 1);
		setStyleDisplayNone(btn);
	},
	k = function (js_src, share_block, btn) {
		scriptIsLoaded(js_src) || loadJS(js_src, function () {
			g(share_block, btn);
		});
	},
	q = function () {
		if (pluso) {
			k(pluso_like_js_src, pluso, btn);
		} else {
			if (ya_share2) {
				k(share_js_src, ya_share2, btn);
			}
		}
	},
	v = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			q();
		});
	};
	if ((pluso || ya_share2) && btn) {
		if (!!getHTTPProtocol()) {
			v();
		} else {
			setStyleDisplayNone(btn);
		}
	}
};
evento.add(window, "load", function () {
	showShareOptionsOnClick();
});
/*!
 * init disqus_thread on scroll
 */
var initDisqusThreadOnScroll = function () {
	var h1 = BALA.one("#h1") || "";
	if (h1) {
		var w = window,
		h = BALA.one("html") || "",
		data_loading = "data-loading",
		disqus_thread = BALA.one("#disqus_thread") || "",
		is_active = "is-active",
		btn = BALA.one("#btn-show-disqus-thread") || "",
		p = w.location.href || "",
		disqus_shortname = disqus_thread ? (disqus_thread.dataset.shortname || "") : "",
		embed_js_src = getHTTPProtocol() + "://" + disqus_shortname + ".disqus.com/embed.js",
		g = function () {
			setStyleDisplayNone(btn);
			disqus_thread.classList.add(is_active);
			!!waypoint && waypoint.destroy();
		},
		k = function () {
			scriptIsLoaded(embed_js_src) || loadJS(embed_js_src, function () {
				g();
			});
		},
		q = function () {
			evento.add(btn, "click", function (e) {
				e.preventDefault();
				e.stopPropagation();
				k();
			});
		},
		v = function () {
			var s = crel("p", "\u041A\u043E\u043C\u043C\u0435\u043D\u0442\u0430\u0440\u0438\u0438 \u0434\u043E\u0441\u0442\u0443\u043F\u043D\u044B \u0442\u043E\u043B\u044C\u043A\u043E \u0432 \u0432\u0435\u0431 \u0432\u0435\u0440\u0441\u0438\u0438 \u044D\u0442\u043E\u0439 \u0441\u0442\u0440\u0430\u043D\u0438\u0446\u044B.");
			removeChildElements(disqus_thread);
			crel(disqus_thread, s);
			disqus_thread.removeAttribute("id");
			setStyleDisplayNone(btn.parentNode);
		};
		if (disqus_thread && btn && disqus_shortname && p) {
			if (!!getHTTPProtocol()) {
				if ("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) {
					q();
				} else {
					if (w.Waypoint) {
						try {
							var waypoint = new Waypoint({
									element : h1,
									handler : function (direction) {
										k();
									}
								});
						} catch (e) {
							console.log(e);
						}
						q();
					} else {
						q();
					}
				}
			} else {
				v();
			}
		}
	}
};
evento.add(window, "load", function () {
	initDisqusThreadOnScroll();
});
/*!
 * init vk-like on click
 */
var initVKOnClick = function () {
	var w = window,
	vk_like = BALA.one("#vk-like") || "",
	btn = BALA.one("#btn-show-vk-like") || "",
	openapi_js_src = getHTTPProtocol() + "://vk.com/js/api/openapi.js?122",
	g = function () {
		try {
			w.VK && (VK.init({
					apiId : (vk_like.dataset.apiid || ""),
					nameTransportPath : "/xd_receiver.htm",
					onlyWidgets : !0
				}), VK.Widgets.Like("vk-like", {
					type : "button",
					height : 24
				}));
			setStyleVisibilityVisible(vk_like);
			setStyleOpacity(vk_like, 1);
			setStyleDisplayNone(btn);
		} catch(e) {
			setStyleVisibilityHidden(vk_like);
			setStyleOpacity(vk_like, 0);
			setStyleDisplayBlock(btn);
		}
	},
	k = function () {
		scriptIsLoaded(openapi_js_src) || loadJS(openapi_js_src, function () {
			g();
		});
	}
	q = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			k();
		});
	};
	if (vk_like && btn) {
		if (!!getHTTPProtocol()) {
				q();
		} else {
			setStyleDisplayNone(btn);
		}
	}
};
evento.add(window, "load", function () {
	initVKOnClick();
});
/*!
 * init manUP.js
 */
var initManupJs = function () {
	var manup_js_src = "/cdn/ManUp.js/0.7/js/manup.fixed.min.js";
	if (!!getHTTPProtocol()) {
		AJAXLoadAndTriggerJs(manup_js_src);
	}
};
evento.add(window, "load", function () {
	initManupJs();
});
/*!
 * show page, finish ToProgress
 */
evento.add(window, "load", function () {
	var a = BALA.one("#container") || "";
	setStyleOpacity(a, 1);
	setImmediate(function () {
		progressBarAvailable && (progressBar.finish(), progressBar.hide());
	});
});
