const electron = require("electron");
const app = electron.app; // a life cycle module
const BrowserWindow = electron.BrowserWindow; // a browser window module
// sending bugs to Electron project.
electron.crashReporter.start({
	productName : "irrverbsscr",
	companyName : "englishextra.github.io",
	submitURL : "https://dashboard.heroku.com/apps/irrverbsscr",
	autoSubmit : true
});
// a global link
// the window will close once the JS object is cleared
var mainWindow = null;
// check if all the app’s windows are closed and shut down the app
app.on("window-all-closed", function () {
	// in OS X stay active until Cmd + Q is pressed
	if (process.platform != "darwin") {
		app.quit();
	}
});
// called when Electron inits and is ready to create a browser window
app.on("ready", function () {
	// create the window
	mainWindow = new BrowserWindow({
			width : 844,
			height : 640,
			icon : "favicon.ico",
			title : "\u041D\u0435\u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u044B\u0435 \u0433\u043B\u0430\u0433\u043E\u043B\u044B"
		});
	// load index.html
	mainWindow.loadURL("file://" + __dirname + "/index.html");
	// open DevTools.
	// mainWindow.webContents.openDevTools();
	// gets executed when window close event is generated
	mainWindow.on("closed", function () {
		// remove the link to the window
		mainWindow = null;
	});
});
